package org.nlopez;

import com.sun.opengl.util.Animator;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

/**
 * ProyectoFinal.java <BR>
 * author: Brian Paul (converted to Java by Ron Cemer and Sven Goethel)
 * <P>
 *
 * This version is equal to Brian Paul's version 1.2 1999/10/21
 */
public class ProyectoFinal implements GLEventListener {

    FCuadrado suelo, entrada, entr_garaje, filos;
    Casa casa;
    Personaje morty;
    FCilindro antena, antena1;
    FEsfera ant1, ant2, ant3;
    FDona manguera1, manguera2;
    FCubo ba1, ba2;
    Nave nave;
    FSilla silla1, silla2, silla3, silla4;

    // Variables de camara
    static double camx = 0;
    static double camy = 1;
    static double camz = 20;

    static double vistx = 0;
    static double visty = 0;
    static double vistz = 0;

    float a = 0;
    static int num_cam = 1;
    int r;

    MCamara mc;
    VistaMouse vm;

    public static void main(String[] args) {
        Frame frame = new Frame("Proyecto Final");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new ProyectoFinal());
        frame.add(canvas);
        frame.setSize(1000, 700);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
    }

    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        gl.glEnable(GL.GL_DEPTH_TEST);

        this.mc = new MCamara();
        drawable.addKeyListener(mc);

        this.vm = new VistaMouse();
        drawable.addMouseListener(vm);
        drawable.addMouseMotionListener(vm);

        this.suelo = new FCuadrado(gl, 0, -0.05f, 0, 38.8f, 26.0f, 90, 0, 0, 0.7f, 0.9f, 0.5f);
        this.casa = new Casa(gl, 0, .9f, 0, .3f, 1.4f, 30, 0);
        this.entrada = new FCuadrado(gl, 2.48f, -0.03f, 15, 1.25f, 8.06f, 90, 0, 0, 0.9f, 1f, 0.85f);
        this.filos = new FCuadrado(gl, 1.42f, -0.02f, 15, .21f, 8.06f, 90, 0, 0, 1f, .45f, .55f);
        this.entr_garaje = new FCuadrado(gl, -16.59f, -0.03f, 17.4f, 5.8f, 5.89f, 90, 0, 0, 0.9f, 1f, 0.85f);
        this.morty = new Personaje(gl, 2.5f, 1.1f, 17f, 1f, 1f, 1f, 0, 180, 0);

        //Antena
        this.antena = new FCilindro(gl, -10.6f, 5.1f, 4.1f, .5f, .5f, 3.2f, 45, 35, 0, 0.8f, 0.8f, 0.8f, .03f, .15f);
        this.antena1 = new FCilindro(gl, -9.8f, 5.9f, 5f, 2f, 2f, .9f, 45, 35, 0, 0.6f, 0.6f, 0.6f, .45f, .01f);
        this.ant1 = new FEsfera(gl, -10.5f, 5.1f, 4.2f, .2f, .22f, .2f, 0, 0, 0, 0.6f, 0.6f, 0.6f);
        this.ant2 = new FEsfera(gl, -9.2f, 6.5f, 5.5f, .19f, .19f, .19f, 0, 0, 0, 0.7f, 0.7f, 0.7f);
        this.ant3 = new FEsfera(gl, -8.9f, 6.9f, 5.9f, .12f, .12f, .12f, 0, 0, 0, 0.8f, 0.8f, 0.8f);

        //Mangueras
        this.manguera1 = new FDona(gl, 7.6f, .58f, 10.4f, 1.1f, .8f, .9f, 0, 90, 0, 0.3f, 0.6f, 0.2f, 0.6f, 0.06f);
        this.manguera2 = new FDona(gl, 7.5f, .58f, 10.6f, 1.1f, .8f, .9f, 0, 90, 0, 0.3f, 0.7f, 0.3f, 0.6f, 0.06f);

        //Gradas
        this.ba1 = new FCubo(gl, 4.83f, 2.05f, 2.5f, .06f, 3.5f, .09f, -48, 0, 0, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);
        this.ba2 = new FCubo(gl, 4.83f, 2.95f, 2.3f, .06f, 3f, .09f, -50, 0, 0, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);

        //Nave
        this.nave = new Nave(gl, -20.5f, .6f, 18f, 1f, 1f, 1f, 0, 180, 0);

        //Sillas
        this.silla1 = new FSilla(gl, -17f, 3.6f, -11.5f, .045f, 1.6f, .1f, 0, 90, 0);
        this.silla2 = new FSilla(gl, -8.f, 3.6f, 1.1f, .045f, 1.6f, .1f, 0, 90, 0);
        this.silla3 = new FSilla(gl, -3.7f, 3.6f, 2.5f, .045f, 1.6f, .1f, 0, 270, 0);
        this.silla4 = new FSilla(gl, -13f, 3.6f, -10.1f, .045f, 1.6f, .1f, 0, 270, 0);

        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!

            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, h, 1.0, 300);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();

        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();
        GLU glu = new GLU();

        if (num_cam == 1) {

            // C�mara que es controlada por teclado la posicion del personaje
            //Morty Smith
            this.morty.Movimiento();
            glu.gluLookAt(morty.x, morty.y + 1, morty.z + 2, camx + vistx, morty.y + camy + visty, vistz  , 0, 1, 0);
        }

        if (num_cam == 2) {

            // Vista Cenital del escenario completo
            camx = 0;
            camy = 65;
            camz = 1;
            glu.gluLookAt(camx, camy, camz - 0.99, 0, 0, 0, 0, 1, 0);
        }

        if (num_cam == 3) {

            //C�mara que gira al rededor de un objeto
            r = 42;
            a = a + 0.005f;

            camx = r * Math.cos(a);
            camz = r * Math.sin(a);

            glu.gluLookAt(camx, 15, camz, casa.x, 0, casa.z + 4, 0, 1, 0);
        }

        if (num_cam == 4) {

            //C�mara que sigue a un objeto
            //Nave de Rick Sanchez
            this.nave.movimiento_nave();
            glu.gluLookAt(nave.x + 4.7, nave.y + 0.9, nave.z + .1, nave.x + 1, nave.y + 1, 0, 0, 1, 0);
        }
        
        if (num_cam == 5) {

            //C�mara panor�mica
            camx = 45;
            camy = 15;
            camz = 55;
            vistx = -15;
            visty = 10;
            vistz = -20;

            glu.gluLookAt(camx, camy, camz, vistx, visty, vistz, 0, 1, 0);
        }

        this.suelo.Dibuja();
        this.casa.Dibuja();
        this.entrada.Dibuja();
        this.entr_garaje.Dibuja();
        this.morty.Dibuja();

        this.filos.x = -this.filos.x + 4.97f;
        this.filos.Dibuja();
        this.filos.x = -this.filos.x + 4.97f;
        this.filos.Dibuja();

        this.antena.Dibuja();
        this.antena1.Dibuja();
        this.ant1.Dibuja();
        this.ant2.Dibuja();
        this.ant3.Dibuja();

        this.manguera1.Dibuja();
        this.manguera2.Dibuja();

        this.ba1.Dibuja();
        this.ba2.Dibuja();

        this.nave.Dibuja();

        this.silla1.Dibuja();
        this.silla2.Dibuja();
        this.silla3.Dibuja();
        this.silla4.Dibuja();

        gl.glFlush();
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
}
