/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class ASala {

    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float ry; //rotacion

    FCubo pared2, pared3, pared5, pared6, marco1, marco2, marco3, marco4, marco5, marco6, marco7, marco8, marco9, marco10,
            alfombra, sobase, solado1, solado2, soesp, mueblet, cajon1, cajon2, mm1, mm2, telem, telep, teleb, teles, lin1,
            lin2, sofbase, soflado1, soflado2, sofesp, cu1, mcu1, f1, f2, f3, espejo1, espejo2, cu2, mcu2, cu3, mcu3,
            cu4, mcu4, cu5, mcu5, cu6, mcu6, cu7, mcu7, cu8, mcu8, cu9, mcu9, cu10, mcu10;
    FCuadrado piso_s;
    FVidrio puerta;
    FMesa mesa, mueble, mes, mes1;
    FCilindro mai, mas, maceta, las, lai, lamp, mai2, mas2;
    FArmarios arm1, arm2, arm3;
    Arbustos planta, planta1, planta2;

    public ASala(GL gl, float x, float y, float z, float w, float h, float p, float ry) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.ry = ry;

        //Paredes y suelo
        this.pared2 = new FCubo(gl, -29.2f, .7f, -.404f, 7.06f, 1.57f, .0076f, 0, 0, 0, 1f, 1f, 0.8f, 1f, 0.97f, 0.75f);//up.x
        this.pared6 = new FCubo(gl, -9.8f, 1.94f, -.404f, 26.2f, .33f, .0076f, 0, 0, 0, 1f, 1f, 0.8f, 1f, 0.97f, 0.75f);//up.y
        this.pared3 = new FCubo(gl, 16.2f, .7f, -.251f, .3f, 1.57f, .149f, 0, 0, 0, 0.8f, 0.4f, 0.3f, 1f, 0.97f, 0.75f);//der
        this.pared5 = new FCubo(gl, 14.9f, .7f, -.107f, 1.6f, 1.57f, .0076f, 0, 0, 0, 1f, 0.97f, 0.75f, 1f, 0.9f, 0.7f);//down
        this.lin1 = new FCubo(gl, 16.2f, .7f, -.403f, .34f, 1.57f, .0076f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//der.u
        this.lin2 = new FCubo(gl, -36, .7f, -.403f, .34f, 1.57f, .0076f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//iz.u
        this.piso_s = new FCuadrado(gl, -10.1f, -.87f, -0.254f, 26.3f, .147f, 90, 0, 0, 0.9f, 0.8f, 0.6f);

        //Filo inferior
        this.f1 = new FCubo(gl, -29.2f, -.82f, -.403f, 7.06f, .06f, .0076f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//up.x
        this.f2 = new FCubo(gl, 16.2f, -.82f, -.251f, .5f, .06f, .149f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//der
        this.f3 = new FCubo(gl, 14.9f, -.82f, -.107f, 1.6f, .06f, .0078f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//down

        //Puerta
        this.puerta = new FVidrio(gl, -9.8f, 0.7f, -.404f, 26.2f, 1.57f, 0, 0, 0);

        //Marcos 
        this.marco1 = new FCubo(gl, -10f, .38f, -.4045f, .3f, 1.27f, .0038f, 0, 0, 0, .75f, 0.75f, 0.75f, .7f, 0.7f, 0.7f);//up.x.1
        this.marco2 = new FCubo(gl, -16.25f, 1.57f, -.4045f, 6.f, .06f, .0038f, 0, 0, 0, .75f, 0.75f, 0.75f, .7f, 0.7f, 0.7f);//up.y.1
        this.marco3 = new FCubo(gl, -9.75f, .38f, -.4025f, .3f, 1.27f, .0038f, 0, 180, 0, .75f, 0.75f, 0.75f, .7f, 0.7f, 0.7f);//up.x.2
        this.marco4 = new FCubo(gl, -3.95f, 1.57f, -.4025f, 6.f, .06f, .0038f, 0, 180, 0, .75f, 0.75f, 0.75f, .7f, 0.7f, 0.7f);//up.y.2

        this.marco5 = new FCubo(gl, -13.55f, .38f, -.107f, .3f, 1.27f, .0079f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//down.x.1
        this.marco6 = new FCubo(gl, -19.35f, 1.59f, -.107f, 5.7f, .06f, .0079f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//down.y.1

        this.marco7 = new FCubo(gl, 13.55f, .38f, -.107f, .3f, 1.27f, .0079f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//down.x.2
        this.marco8 = new FCubo(gl, 9.35f, 1.59f, -.107f, 4.3f, .06f, .0079f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//down.y.2

        this.marco9 = new FCubo(gl, -36, .38f, -.33f, .45f, 1.27f, .0019f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//iz.x
        this.marco10 = new FCubo(gl, -36, 1.59f, -.29f, .4f, .059f, .04f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//iz.y

        //Detalles
        //Alfombra y muebles
        this.alfombra = new FCubo(gl, -4.4f, -.87f, -0.274f, 5.2f, .06f, .075f, 0, 0, 0, 0.9f, 1f, 0.5f, 0.9f, 1f, 0.5f);
        this.mesa = new FMesa(gl, 1.5f, 1.7f, -.299f, .45f, 1.24f, .014f, 0, 0, 0);
        this.mueble = new FMesa(gl, -29.7f, 1.5f, -.375f, .23f, 1.04f, .008f, 0, 0, 0);
        this.mes = new FMesa(gl, -15.5f, 2.2f, -.20f, .10f, 1.3f, .004f, 0, 0, 0);
        this.mes1 = new FMesa(gl, -18.5f, 2.2f, -.292f, .10f, 1.3f, .01f, 0, 0, 0);
        this.arm1 = new FArmarios(gl, 12.5f, 1.85f, -.145f, .15f, 1.24f, .0047f, 0, 180, 0);
        this.arm2 = new FArmarios(gl, -32.9f, 3.29f, -.176f, .10f, 1.89f, .0075f, 0, 0, 0);
        this.arm3 = new FArmarios(gl, -32.9f, -3.21f, -.154f, .10f, 1.89f, .0075f, 180, 0, 0);

        //Sofa celeste
        this.sobase = new FCubo(gl, -16.f, -.55f, -0.277f, 2f, .2f, .06f, 0, 0, 0, 0.65f, 0.99f, 0.75f, 0.6f, 1f, 0.7f);
        this.soesp = new FCubo(gl, -17.8f, -.4f, -0.277f, .77f, .55f, .061f, 0, 180, 0, 0.67f, 1f, 0.77f, 0.8f, 1f, 0.9f);
        this.solado1 = new FCubo(gl, -16.f, -.6f, -0.22f, 2f, .47f, .0061f, 0, 0, 0, 0.7f, 0.99f, 0.8f, 0.65f, 1f, 0.75f);
        this.solado2 = new FCubo(gl, -16.f, -.6f, -0.335f, 2f, .47f, .0061f, 0, 0, 0, 0.7f, 0.99f, 0.8f, 0.65f, 1f, 0.75f);

        //Sofa azul
        this.sofbase = new FCubo(gl, -11.f, -.55f, -0.157f, 2f, .2f, .015f, 0, 0, 0, 0.35f, 0.59f, 1f, 0.3f, .5f, 1f);
        this.sofesp = new FCubo(gl, -11.f, -.4f, -0.14f, 2f, .55f, .0061f, 0, 0, 0, 0.3f, 0.69f, 0.86f, 0.35f, .6f, 0.86f);
        this.soflado1 = new FCubo(gl, -12.8f, -.6f, -0.157f, .4f, .47f, .016f, 0, 0, 0, 0.37f, .6f, 0.87f, 0.3f, .6f, 0.8f);
        this.soflado2 = new FCubo(gl, -9.2f, -.6f, -0.157f, .4f, .47f, .016f, 0, 0, 180, 0.37f, .6f, 0.87f, 0.3f, .6f, 0.8f);

        //"Librero"
        this.mueblet = new FCubo(gl, 14f, -1f, -.277f, 1.81f, .87f, .038f, 180, 0, 180, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cajon1 = new FCubo(gl, 13.9f, -0.35f, -.277f, 1.81f, .134f, .034f, 180, 0, 0, 0.7f, 0.5f, 0.3f, 0.7f, 0.5f, 0.3f);
        this.cajon2 = new FCubo(gl, 13.9f, -.65f, -.277f, 1.81f, .134f, .034f, 180, 0, 0, 0.7f, 0.5f, 0.3f, 0.7f, 0.5f, 0.3f);
        this.mm1 = new FCubo(gl, 13.89f, -0.35f, -.277f, 1.81f, .017f, .003f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.75f, 0.75f, 0.75f);
        this.mm2 = new FCubo(gl, 13.89f, -.65f, -.277f, 1.81f, .017f, .003f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.75f, 0.75f, 0.75f);

        //Television
        this.telem = new FCubo(gl, 14f, .3f, -.277f, .21f, .29f, .032f, 180, 0, 180, 0.75f, 0.75f, 0.75f, 0.7f, 0.7f, 0.7f);
        this.telep = new FCubo(gl, 13.9f, .3f, -.277f, .21f, .25f, .03f, 180, 0, 180, 0f, 0f, 0f, 0f, 0f, 0f);
        this.teles = new FCubo(gl, 14f, .1f, -.277f, .21f, .29f, .003f, 180, 0, 180, 0.75f, 0.75f, 0.75f, 0.7f, 0.7f, 0.7f);
        this.teleb = new FCubo(gl, 14f, -.15f, -.277f, .31f, .04f, .012f, 180, 0, 180, 0.74f, 0.74f, 0.74f, 0.71f, 0.71f, 0.71f);

        //Lampara
        this.lai = new FCilindro(gl, -16.58f, -0.22f, -.198f, 2.4f, .03f, .12f, 0, 0, 0, .91f, .91f, 0.3f, 0.25f, 0.15f);
        this.las = new FCilindro(gl, -16.58f, -0.1f, -.198f, 2.4f, .03f, .12f, 0, 0, 0, .91f, .91f, 0.3f, 0.15f, 0.25f);
        this.lamp = new FCilindro(gl, -16.58f, 0.03f, -.198f, 2.4f, .03f, .2f, 0, 0, 0, .89f, .89f, 0.89f, 0.18f, 0.28f);

        //Plantas
        this.maceta = new FCilindro(gl, 13.2f, -.68f, -.354f, 4.f, .04f, .77f, 0, 0, 0, 0.9f, 0.6f, 0.4f, 0.57f, 0.4f);
        this.planta = new Arbustos(gl, 11.8f, .18f, -.356f, 1.9f, .4f, .022f, 0, 0, 0);//maceta
        this.mai = new FCilindro(gl, -32.58f, -0.42f, -.384f, 2.4f, .03f, .12f, 0, 0, 0, 0.55f, 0.85f, 0.95f, 0.25f, 0.15f);
        this.mas = new FCilindro(gl, -32.58f, -0.3f, -.384f, 2.4f, .03f, .12f, 0, 0, 0, 0.55f, 0.85f, 0.95f, 0.15f, 0.25f);
        this.planta1 = new Arbustos(gl, -32.58f, -0.1f, -.385f, .15f, .5f, .003f, 0, 0, 0);//florero.iz
        this.mai2 = new FCilindro(gl, 14.2f, -0.12f, -.134f, 2.4f, .03f, .12f, 0, 0, 0, 0.55f, 0.85f, 0.95f, 0.25f, 0.15f);
        this.mas2 = new FCilindro(gl, 14.2f, 0.0f, -.134f, 2.4f, .03f, .12f, 0, 0, 0, 0.55f, 0.85f, 0.95f, 0.15f, 0.25f);
        this.planta2 = new Arbustos(gl, 14.1f, 0.1f, -.135f, .15f, .5f, .003f, 0, 0, 0);//florero.der

        //Cuadros
        this.espejo1 = new FCubo(gl, -35.5f, .1f, -.357f, .21f, .335f, .008f, 0, 0, 0, 0.7f, 1f, 1f, 0.7f, 1f, 1f);
        this.espejo2 = new FCubo(gl, -35.51f, .1f, -.357f, .21f, .355f, .01f, 0, 0, 0, 0.55f, 0.85f, 0.95f, 0.5f, 0.8f, 0.9f);
        this.cu1 = new FCubo(gl, -3.1f, .95f, -.119f, 1.91f, .225f, .002f, 0, 0, 0, 0f, 0.8f, 0.2f, 0f, 0.8f, .2f);//down
        this.mcu1 = new FCubo(gl, -3.1f, .95f, -.118f, 1.98f, .255f, .002f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu2 = new FCubo(gl, -28.1f, .95f, -.391f, 1.41f, .225f, .002f, 0, 0, 0, 0.7f, 0.8f, 0.1f, 0.7f, 0.8f, 0.1f);//up.iz
        this.mcu2 = new FCubo(gl, -28.1f, .95f, -.392f, 1.58f, .255f, .002f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu3 = new FCubo(gl, 9.1f, .95f, -.391f, 1.41f, .225f, .002f, 0, 0, 0, 0.9f, 0.7f, 0.8f, 0.9f, 0.7f, 0.8f);//up.der
        this.mcu3 = new FCubo(gl, 9.1f, .95f, -.392f, 1.58f, .255f, .002f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu4 = new FCubo(gl, 16.08f, .91f, -.18f, .21f, .195f, .005f, 0, 0, 0, 0.95f, 0.99f, 0.55f, 0.9f, 1f, 0.5f);//p.1
        this.mcu4 = new FCubo(gl, 16.09f, .91f, -.18f, .21f, .225f, .007f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu5 = new FCubo(gl, 16.08f, .91f, -.2f, .21f, .195f, .005f, 0, 0, 0, 0.95f, 0.99f, 0.85f, 0.9f, 1f, 0.8f);//p.2
        this.mcu5 = new FCubo(gl, 16.09f, .91f, -.2f, .21f, .225f, .007f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu6 = new FCubo(gl, 16.08f, .91f, -.16f, .21f, .195f, .005f, 0, 0, 0, 0.95f, 1f, 0.65f, 0.95f, 1f, 0.6f);//p.3
        this.mcu6 = new FCubo(gl, 16.09f, .91f, -.16f, .21f, .225f, .007f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu7 = new FCubo(gl, -19.68f, .03f, -.249f, .21f, .165f, .003f, 0, 0, 0, 0.9f, 1f, 0.6f, 0.9f, 1f, 0.6f);//m.1
        this.mcu7 = new FCubo(gl, -19.67f, .03f, -.249f, .21f, .195f, .005f, 0, 0, 0, 0.45f, 0.65f, 1f, 0.4f, .6f, 1f);
        this.cu8 = new FCubo(gl, -19.68f, -.03f, -.268f, .21f, .105f, .003f, 0, 0, 0, 0.85f, 1f, 0.65f, 0.85f, 1f, 0.65f);//m.2
        this.mcu8 = new FCubo(gl, -19.67f, -.03f, -.268f, .21f, .135f, .005f, 0, 0, 0, 0.45f, 0.65f, 1f, 0.4f, .6f, 1f);
        this.cu9 = new FCubo(gl, -19.68f, -.03f, -.287f, .21f, .105f, .003f, 0, 0, 0, 0.9f, 1f, 0.5f, 0.95f, 1f, 0.5f);//m.3
        this.mcu9 = new FCubo(gl, -19.67f, -.03f, -.287f, .21f, .135f, .005f, 0, 0, 0, 0.45f, 0.65f, 1f, 0.4f, .6f, 1f);
        this.cu10 = new FCubo(gl, -19.68f, .03f, -.306f, .21f, .165f, .003f, 0, 0, 0, 1f, 1f, 0.75f, 1f, 1f, 0.7f);//m.4
        this.mcu10 = new FCubo(gl, -19.67f, .03f, -.306f, .21f, .195f, .005f, 0, 0, 0, 0.45f, 0.65f, 1f, 0.4f, .6f, 1f);
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glScalef(this.w, this.h, this.p);

        this.pared2.x = -this.pared2.x - 19.8f;
        this.pared2.Dibuja();
        this.pared2.x = -this.pared2.x - 19.8f;
        this.pared2.Dibuja();
        this.pared6.Dibuja();
        this.pared3.Dibuja();
        this.pared5.Dibuja();
        this.piso_s.Dibuja();

        this.f1.x = -this.f1.x - 19.8f;
        this.f1.Dibuja();
        this.f1.x = -this.f1.x - 19.8f;
        this.f1.Dibuja();
        this.f2.Dibuja();
        this.f3.Dibuja();

        this.lin1.Dibuja();
        this.lin2.Dibuja();

        this.puerta.Dibuja();

        this.marco1.x = -this.marco1.x - 31.8f;
        this.marco1.Dibuja();
        this.marco1.x = -this.marco1.x - 31.8f;
        this.marco1.Dibuja();
        this.marco2.y = -this.marco2.y + .74f;
        this.marco2.Dibuja();
        this.marco2.y = -this.marco2.y + .74f;
        this.marco2.Dibuja();

        this.marco3.x = -this.marco3.x - 7.6f;
        this.marco3.Dibuja();
        this.marco3.x = -this.marco3.x - 7.6f;
        this.marco3.Dibuja();
        this.marco4.y = -this.marco4.y + .74f;
        this.marco4.Dibuja();
        this.marco4.y = -this.marco4.y + .74f;
        this.marco4.Dibuja();

        this.marco5.x = -this.marco5.x - 38.7f;
        this.marco5.Dibuja();
        this.marco5.x = -this.marco5.x - 38.7f;
        this.marco5.Dibuja();
        this.marco6.Dibuja();

        this.marco7.x = -this.marco7.x + 18.4f;
        this.marco7.Dibuja();
        this.marco7.x = -this.marco7.x + 18.4f;
        this.marco7.Dibuja();
        this.marco8.Dibuja();

        this.marco9.z = -this.marco9.z - .58f;
        this.marco9.Dibuja();
        this.marco9.z = -this.marco9.z - .58f;
        this.marco9.Dibuja();
        this.marco10.Dibuja();

        this.alfombra.Dibuja();
        this.mueble.Dibuja();
        this.mesa.Dibuja();
        this.mes.Dibuja();
        this.mes1.Dibuja();

        this.sobase.Dibuja();
        this.soesp.Dibuja();
        this.solado1.Dibuja();
        this.solado2.Dibuja();

        this.sofbase.Dibuja();
        this.sofesp.Dibuja();
        this.soflado1.Dibuja();
        this.soflado2.Dibuja();

        this.mueblet.Dibuja();
        this.cajon1.Dibuja();
        this.cajon2.Dibuja();
        this.mm1.Dibuja();
        this.mm2.Dibuja();

        this.telem.Dibuja();
        this.telep.Dibuja();
        this.teles.Dibuja();
        this.teleb.Dibuja();

        this.maceta.Dibuja();
        this.planta.Dibuja();

        this.mai.Dibuja();
        this.mas.Dibuja();
        this.planta1.Dibuja();

        this.mai2.Dibuja();
        this.mas2.Dibuja();
        this.planta2.Dibuja();

        this.lai.Dibuja();
        this.las.Dibuja();
        this.lamp.Dibuja();

        this.arm1.Dibuja1();
        this.arm2.Dibuja1();
        this.arm3.Dibuja1();

        this.espejo1.Dibuja();
        this.espejo2.Dibuja();
        this.cu1.Dibuja();
        this.mcu1.Dibuja();
        this.cu2.Dibuja();
        this.mcu2.Dibuja();
        this.cu3.Dibuja();
        this.mcu3.Dibuja();
        this.cu4.Dibuja();
        this.mcu4.Dibuja();
        this.cu5.Dibuja();
        this.mcu5.Dibuja();
        this.cu6.Dibuja();
        this.mcu6.Dibuja();
        this.cu7.Dibuja();
        this.mcu7.Dibuja();
        this.cu8.Dibuja();
        this.mcu8.Dibuja();
        this.cu9.Dibuja();
        this.mcu9.Dibuja();
        this.cu10.Dibuja();
        this.mcu10.Dibuja();

        gl.glEnd();
        gl.glPopMatrix();

    }
}
