/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class Arbustos {

    GL gl;
    float x, y, z; //traslacion
    float w, h, d; //escalado
    float rx, ry, rz; //rotacion

    FEsfera a1, a2, a3, a4, a5, a6, a7, a8, a9;

    public Arbustos(GL gl, float x, float y, float z, float w, float h, float d, float rx, float ry, float rz) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.d = d;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;

        this.a1 = new FEsfera(gl, 0.05f, 0, -0.2f, -0.35f, 0.35f, 0.35f, 0, 0, 0, 0.35f, 0.85f, 0.15f);
        this.a2 = new FEsfera(gl, 1, 0, -0.3f, 0.3f, 0.3f, 0.3f, 0, 0, 0, 0.25f, 0.75f, 0.05f);
        this.a3 = new FEsfera(gl, 0.3f, 0.5f, 0.4f, 0.43f, 0.43f, 0.43f, 0, 0, 0, 0.4f, 0.8f, 0.35f);
        this.a4 = new FEsfera(gl, 0.5f, 0.1f, 0.9f, 0.3f, 0.3f, 0.3f, 0, 0, 0, 0.35f, 0.8f, 0.1f);
        this.a5 = new FEsfera(gl, 1.5f, 0f, 0.15f, 0.41f, 0.41f, 0.41f, 0, 0, 0, 0.3f, 0.8f, 0.15f);
        this.a6 = new FEsfera(gl, 0.5f, 0.3f, -0.3f, 0.33f, 0.33f, 0.33f, 0, 0, 0, 0.3f, 0.85f, 0.1f);
        this.a7 = new FEsfera(gl, 1.2f, 0.4f, 0.5f, 0.45f, 0.45f, 0.45f, 0, 0, 0, 0.23f, 0.78f, 0.1f);
        this.a8 = new FEsfera(gl, 0.09f, 0f, 0.5f, -0.4f, 0.4f, 0.4f, 0, 0, 0, 0.4f, 0.8f, 0.3f);
        this.a9 = new FEsfera(gl, 1, 0.1f, -0.1f, 0.48f, 0.48f, 0.48f, 0, 0, 0, 0.3f, 0.8f, 0.1f);
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);

        gl.glScalef(this.w, this.h, this.d);

        this.a1.Dibuja();
        this.a2.Dibuja();
        this.a3.Dibuja();
        this.a4.Dibuja();
        this.a5.Dibuja();
        this.a6.Dibuja();
        this.a7.Dibuja();
        this.a8.Dibuja();
        this.a9.Dibuja();

        gl.glPopMatrix();
    }
}
