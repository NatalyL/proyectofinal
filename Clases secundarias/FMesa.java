/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class FMesa {

    GL gl;
    float x, y, z; //traslacion
    float w, h, d; //escalado
    float rx, ry, rz; //rotacion

    FCubo a1, a2, p1, p2;

    public FMesa(GL gl, float x, float y, float z, float w, float h, float d, float rx, float ry, float rz) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.d = d;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;

        this.a1 = new FCubo(gl, -14.6f, -1.9f, 1.46f, 10.21f, .01f, 3.5f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.a2 = new FCubo(gl, -14.6f, -1.85f, 1.46f, 10.21f, .03f, 3.8f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.p1 = new FCubo(gl, -5.6f, -2.15f, -1.86f, 1.21f, .27f, .5f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.p2 = new FCubo(gl, -5.6f, -2.15f, 4.56f, 1.21f, .27f, .5f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);

        gl.glScalef(this.w, this.h, this.d);

        this.a1.Dibuja();
        this.a2.Dibuja();

        this.p1.x = -this.p1.x - 28.9f;
        this.p1.Dibuja();
        this.p1.x = -this.p1.x - 28.9f;
        this.p1.Dibuja();

        this.p2.x = -this.p2.x - 28.9f;
        this.p2.Dibuja();
        this.p2.x = -this.p2.x - 28.9f;
        this.p2.Dibuja();

        gl.glPopMatrix();
    }
}
