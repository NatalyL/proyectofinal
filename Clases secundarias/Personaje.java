/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class Personaje {

    GL gl;
    float x, y, z; //traslacion
    float w, h, d; //escalado
    float rx, ry, rz; //rotacion

    FCilindro cuello, torax, brazoI, brazoD, mangaI, mangaD, piernaI, piernaD;
    FEsfera cabeza, cabello, manoI, manoD, pieI, pieD, cadera, ojo, pupila;
    FCirculo sonrisa;

    public Personaje(GL gl, float x, float y, float z, float w, float h, float d, float rx, float ry, float rz) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.d = d;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;

        this.cabeza = new FEsfera(gl, 0, .64f, 0, .2f, .22f, .2f, 0, 0, 0, 1f, 0.9f, 0.7f);

        this.ojo = new FEsfera(gl, 0.069f, .66f, .17f, .052f, .052f, .052f, 0, 0, 0, 1, 1, 1);
        this.pupila = new FEsfera(gl, 0.069f, .66f, .22f, .01f, .01f, .01f, 0, 0, 0, 0, 0, 0);

        this.cabello = new FEsfera(gl, 0, .66f, -0.05f, .22f, .24f, .22f, 0, 0, 0, 0.7f, 0.3f, 0.1f);

        this.cuello = new FCilindro(gl, 0, .4f, 0, .2f, .2f, .15f, 0, 0, 0, 1f, 0.9f, 0.7f, 0.3f, 0.3f);

        this.torax = new FCilindro(gl, 0, -0.25f, 0, .4f, .4f, .65f, 0, 0, 0, 1, 1, 0.4f, 0.4f, 0.4f);

        this.mangaD = new FCilindro(gl, 0.1f, .37f, 0, .2f, .2f, .5f, 0, 150, 0, 1, 1, 0.4f, 0.3f, 0.2f);
        this.mangaI = new FCilindro(gl, -0.1f, .37f, 0, .2f, .2f, .5f, 0, -150, 0, 1, 1, 0.4f, 0.3f, 0.2f);

        this.brazoD = new FCilindro(gl, .3f, -.05f, 0, 0.2f, 0.2f, .25f, 120, -45, 0, 1f, 0.9f, 0.7f, 0.15f, 0.15f);
        this.brazoI = new FCilindro(gl, -.3f, -.05f, 0, 0.2f, 0.2f, .25f, 120, 45, 0, 1f, 0.9f, 0.7f, 0.15f, 0.15f);

        this.manoD = new FEsfera(gl, .1f, -.15f, .15f, .05f, .05f, .05f, 0, 0, 0, 1f, 0.9f, 0.7f);
        this.manoI = new FEsfera(gl, -.1f, -.15f, .15f, .05f, .05f, .05f, 0, 0, 0, 1f, 0.9f, 0.7f);

        this.cadera = new FEsfera(gl, 0, -0.25f, 0, .13f, .18f, .08f, 0, 0, 0, 0.2f, 0.3f, 0.6f);

        this.piernaD = new FCilindro(gl, .1f, -1.05f, 0, .2f, .25f, .85f, 0, 0, 0, 0.2f, 0.3f, 0.6f, 0.3f, 0.2f);
        this.piernaI = new FCilindro(gl, -.1f, -1.05f, 0, .2f, .25f, .85f, 0, 0, 0, 0.2f, 0.3f, 0.6f, 0.3f, 0.2f);

        this.pieD = new FEsfera(gl, .1f, -1.1f, 0.05f, 0.05f, 0.05f, 0.1f, 0, 0, 0, 0.3f, 0.3f, 0.3f);
        this.pieI = new FEsfera(gl, -.1f, -1.1f, 0.05f, 0.05f, 0.05f, 0.1f, 0, 0, 0, 0.3f, 0.3f, 0.3f);

        this.sonrisa = new FCirculo(gl, 0, .56f, .18f, .09f, .05f, .09f, 0, 0, 0, 1, 1, 1);

    }

    public void Dibuja() {

        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);

        gl.glScalef(this.w, this.h, this.d);

        cabeza.Dibuja();
        cabello.Dibuja();
        sonrisa.DibujaMedio();

        this.ojo.x = -this.ojo.x;
        this.ojo.Dibuja();
        this.ojo.x = -this.ojo.x;
        this.ojo.Dibuja();
        this.pupila.x = -this.pupila.x;
        this.pupila.Dibuja();
        this.pupila.x = -this.pupila.x;
        this.pupila.Dibuja();

        this.cuello.Dibuja();

        this.torax.Dibuja();

        this.brazoD.Dibuja();
        this.brazoI.Dibuja();

        this.mangaD.Dibuja();
        this.mangaI.Dibuja();

        this.manoD.Dibuja();
        this.manoI.Dibuja();

        this.cadera.Dibuja();

        this.piernaD.Dibuja();
        this.piernaI.Dibuja();

        this.pieD.Dibuja();
        this.pieI.Dibuja();

        gl.glPopMatrix();
    }

    public void Movimiento() {

        this.x = (float) ProyectoFinal.camx; 
//        this.ry = (float) ProyectoFinal.vistx; 
        
        this.z = (float) ProyectoFinal.camz;
    }

}
