/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class AComedor {

    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float ry; //rotacion

    FCubo pared1, pared2, pared3, pared4, pared5, pared6, marco1, marco2, marco3, marco4, b1, b2, mesa, pata1, pata2,
            cu1, cu2, cu3, cu4, cu5, mueble, cajon1, cajon2, cajon3, mm1, mm2, mm3, relojero, rel1, rel2, mane1, mane2, pend,
            mcu1, mcu2, mcu3, mcu4, mcu5, lin1, lin2, lin3, lin4, f1, f2, f3, f4;
    FCuadrado piso_com, cortina;
    FVidrio ventana;
    FSilla silla1, silla2, silla3, silla4;
    FVidrio vidrio;
    FPrismaTriIguales rel;
    FCirculo reloj, pen;
    FCilindro cort;
    FEsfera cor1, cor2;
    FDona cora1, cora2, cora3;

    public AComedor(GL gl, float x, float y, float z, float w, float h, float p, float ry) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.ry = ry;

        //Paredes y suelo
        this.pared1 = new FCubo(gl, -35.98f, .7f, .147f, .3f, 1.57f, .047f, 0, 0, 0, 1f, 0.97f, 0.75f, 1f, 1f, 0.8f);//iz
        this.pared2 = new FCubo(gl, -30.5f, .7f, -.107f, 5.44f, 1.57f, .0076f, 0, 0, 0, 1f, 0.97f, 0.75f, 1f, 0.97f, 0.75f);//up.x
        this.pared6 = new FCubo(gl, -19.2f, 1.94f, -.107f, 16.24f, .33f, .0076f, 0, 0, 0, 1f, 0.97f, 0.75f, 1f, 0.97f, 0.75f);//up.y
        this.pared3 = new FCubo(gl, -3.2f, .7f, .171f, .3f, 1.57f, .017f, 0, 0, 0, 1f, 0.9f, 0.7f, 1f, 0.97f, 0.75f);//der
        this.pared4 = new FCubo(gl, -32.1f, .7f, .193f, 3.81f, 1.57f, .0076f, 0, 0, 0, 1f, 0.97f, 0.75f, 1f, 1f, 0.8f);//down.x
        this.pared5 = new FCubo(gl, -19.2f, 1.94f, .193f, 16.2f, .33f, .0076f, 0, 0, 0, 1f, 0.97f, 0.75f, 1f, 1f, 0.8f);//down.y
        this.lin1 = new FCubo(gl, -35.97f, .7f, .192f, .3f, 1.57f, .0076f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//iz.d
        this.lin2 = new FCubo(gl, -35.97f, .7f, -.107f, .3f, 1.57f, .0078f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//iz.u
        this.lin3 = new FCubo(gl, -3.2f, .7f, .192f, .34f, 1.57f, .0076f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//der.d
        this.lin4 = new FCubo(gl, -3.2f, .7f, -.106f, .34f, 1.57f, .0076f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//der.u
        this.piso_com = new FCuadrado(gl, -19.8f, -.87f, 0.04f, 16.54f, .147f, 90, 0, 0, 0.92f, 0.85f, 0.6f);

        //Filo inferior
        this.f1 = new FCubo(gl, -35.97f, -.82f, .147f, .3f, .06f, .047f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//iz
        this.f2 = new FCubo(gl, -30.5f, -.82f, -.107f, 5.44f, .06f, .0078f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//up.x
        this.f3 = new FCubo(gl, -3.2f, -.82f, .171f, .5f, .06f, .017f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//der
        this.f4 = new FCubo(gl, -32.1f, -.82f, .19f, 3.81f, .06f, .0076f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//down.x

        //Ventana
        this.ventana = new FVidrio(gl, -19.2f, 0.7f, .193f, 16.2f, 1.57f, 0, 0, 0);
        this.b1 = new FCubo(gl, -22.5f, .69f, .193f, .15f, 0.9f, .007f, 0, 0, 0, 1f, 1f, 1f, 0.7f, 0.5f, 0.3f);//down.x
        this.b2 = new FCubo(gl, -19.2f, 1.05f, .193f, 9.2f, .04f, .007f, 0, 0, 0, 1f, 1f, 1f, 0.7f, 0.5f, 0.3f);//down.y

        //Marcos puertas
        this.marco1 = new FCubo(gl, -3.2f, .38f, .08f, .45f, 1.27f, .0019f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//iz.x
        this.marco2 = new FCubo(gl, -3.2f, 1.59f, .119f, .4f, .059f, .037f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//iz.y

        //Marcos ventanas
        this.marco3 = new FCubo(gl, -28.1f, .69f, .193f, .3f, 0.9f, .0079f, 0, 0, 0, 1f, 1f, 1f, 0.7f, 0.5f, 0.3f);//down.x
        this.marco4 = new FCubo(gl, -19.2f, 1.59f, .193f, 9.2f, .06f, .0079f, 0, 0, 0, 1f, 1f, 1f, 0.7f, 0.5f, 0.3f);//down.y

        //Detalles
        //Mesa
        this.mesa = new FCubo(gl, -19.8f, -.3f, 0.06f, 5.81f, .13f, .038f, 0, 0, 0, 0.95f, 0.99f, 0.55f, 0.9f, 1f, 0.5f);
        this.pata1 = new FCubo(gl, -24.8f, -.65f, 0.035f, .15f, 0.24f, .003f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.pata2 = new FCubo(gl, -24.8f, -.65f, 0.09f, .15f, 0.24f, .003f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);

        //Sillas
        this.silla1 = new FSilla(gl, -21.f, 1.9f, .013f, .15f, 1.24f, .003f, 0, 0, 0);
        this.silla2 = new FSilla(gl, -19.5f, 1.9f, .11f, .15f, 1.24f, .003f, 0, 180, 0);
        this.silla3 = new FSilla(gl, -15.f, 1.9f, .013f, .15f, 1.24f, .003f, 0, 0, 0);
        this.silla4 = new FSilla(gl, -25.5f, 1.9f, .11f, .15f, 1.24f, .003f, 0, 180, 0);

        //Cuadros
        this.cu1 = new FCubo(gl, -35.48f, 1f, .133f, .21f, .225f, .007f, 0, 0, 0, 1f, 0.8f, 0.6f, 1f, 0.8f, 0.6f);
        this.mcu1 = new FCubo(gl, -35.49f, 1f, .133f, .21f, .255f, .009f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu2 = new FCubo(gl, -31.1f, .95f, .18f, 1.11f, .225f, .002f, 0, 0, 0, 0.4f, 0.8f, 0.5f, 0.4f, 0.8f, 0.5f);
        this.mcu2 = new FCubo(gl, -31.1f, .95f, .181f, 1.18f, .255f, .002f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu3 = new FCubo(gl, -7.1f, .95f, .18f, 1.11f, .225f, .002f, 0, 0, 0, 1f, 0.6f, 0f, 0.8f, 0.85f, 1f);
        this.mcu3 = new FCubo(gl, -7.1f, .95f, .181f, 1.18f, .255f, .002f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu4 = new FCubo(gl, -3.8f, 1f, .02f, .21f, .225f, .007f, 0, 0, 0, 0.95f, 0.99f, 0.55f, 0.9f, 1f, 0.5f);
        this.mcu4 = new FCubo(gl, -3.7f, 1f, .02f, .21f, .255f, .009f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu5 = new FCubo(gl, -35.48f, 1f, 0f, .21f, .225f, .007f, 0, 0, 0, .5f, 1f, 0.4f, .5f, 1f, 0.4f);
        this.mcu5 = new FCubo(gl, -35.49f, 1f, 0f, .21f, .255f, .009f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);

        //Mueble
        this.mueble = new FCubo(gl, -34f, 0f, -.057f, 1.81f, .87f, .038f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cajon1 = new FCubo(gl, -33.9f, 0.6f, -.057f, 1.81f, .174f, .034f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.7f, 0.5f, 0.3f);
        this.cajon2 = new FCubo(gl, -33.9f, -0.6f, -.057f, 1.81f, .174f, .034f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.7f, 0.5f, 0.3f);
        this.cajon3 = new FCubo(gl, -33.9f, 0f, -.057f, 1.81f, .174f, .034f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.7f, 0.5f, 0.3f);
        this.mm1 = new FCubo(gl, -33.89f, 0.6f, -.057f, 1.81f, .017f, .003f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.75f, 0.75f, 0.75f);
        this.mm2 = new FCubo(gl, -33.89f, -0.6f, -.057f, 1.81f, .017f, .003f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.75f, 0.75f, 0.75f);
        this.mm3 = new FCubo(gl, -33.89f, 0f, -.057f, 1.81f, .017f, .003f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.75f, 0.75f, 0.75f);

        //Relojero
        this.relojero = new FCubo(gl, -5f, 0.1f, -.08f, 1.71f, 1.29f, .019f, 180, 180, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.vidrio = new FVidrio(gl, -6.78f, 0.4f, -.08f, .007f, .32f, 0, 270, 0);
        this.rel = new FPrismaTriIguales(gl, -5f, 1.67f, -.08f, .0199f, .3f, 1.66f, 0, 90, 0, 0.67f, 0.45f, 0.23f, 0.67f, 0.45f, 0.23f);
        this.reloj = new FCirculo(gl, -6.8f, 1.3f, -.08f, .01f, .23f, .36f, 0, 90, 0, 1f, 1f, 1f);
        this.rel1 = new FCubo(gl, -5f, 0.9f, -.08f, 1.75f, .05f, .021f, 0, 0, 0, 0.65f, 0.45f, 0.25f, 0.6f, 0.4f, 0.2f);
        this.rel2 = new FCubo(gl, -5f, -0.1f, -.08f, 1.75f, .05f, .021f, 0, 0, 0, 0.65f, 0.45f, 0.25f, 0.6f, 0.4f, 0.2f);
        this.pend = new FCubo(gl, -5.1f, 0.4f, -.08f, 1.72f, .22f, .0011f, 0, 0, 0, 0.95f, 0.99f, 0.55f, 0.9f, 1f, 0.5f);
        this.pen = new FCirculo(gl, -6.8f, .2f, -.08f, .0025f, .06f, .36f, 0, 90, 0, 0.9f, 1f, 0.5f);
        this.mane1 = new FCubo(gl, -5.1f, 1.3f, -.079f, 1.75f, .005f, .002f, 0, 0, 0, 0f, 0f, 0f, 0f, 0f, 0f);
        this.mane2 = new FCubo(gl, -5.1f, 1.36f, -.08f, 1.75f, .07f, .0002f, 0, 0, 0, 0f, 0f, 0f, 0f, 0f, 0f);

        //Cortina
        this.cort = new FCilindro(gl, -28.6f, 1.7f, 0.179f, .2f, .01f, 19.15f, 0, 90, 0, 0.7f, 0.7f, 0.7f, 0.3f, 0.3f);
        this.cor1 = new FEsfera(gl, -29.05f, 1.7f, .179f, .6f, .06f, .006f, 0, 0, 0, 0.6f, 0.6f, 0.6f);
        this.cor2 = new FEsfera(gl, -9.3f, 1.7f, .179f, .6f, .06f, .006f, 0, 0, 0, 0.6f, 0.6f, 0.6f);
        this.cora1 = new FDona(gl, -25.61f, 1.7f, .179f, .01f, .1f, 1.1f, 0, 90, 0, 1f, 1f, 0.4f, 0.7f, 0.1f);
        this.cora2 = new FDona(gl, -27.61f, 1.7f, .179f, .01f, .1f, 1.1f, 0, 90, 0, 1f, 1f, 0.4f, 0.7f, 0.1f);
        this.cora3 = new FDona(gl, -23.61f, 1.7f, .179f, .01f, .1f, 1.1f, 0, 90, 0, 1f, 1f, 0.4f, 0.7f, 0.1f);
        this.cortina = new FCuadrado(gl, -25.61f, 0.6f, 0.179f, 2.54f, 1.07f, 0, 0, 0, 0.99f, 0.8f, 0.8f);
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glScalef(this.w, this.h, this.p);

        this.pared1.Dibuja();
        this.pared2.x = -this.pared2.x - 38.84f;
        this.pared2.Dibuja();
        this.pared2.x = -this.pared2.x - 38.84f;
        this.pared2.Dibuja();
        this.pared6.Dibuja();
        this.pared3.Dibuja();
        this.pared4.x = -this.pared4.x - 38.84f;
        this.pared4.Dibuja();
        this.pared4.x = -this.pared4.x - 38.84f;
        this.pared4.Dibuja();
        this.pared5.y = -this.pared5.y + 1.4f;
        this.pared5.Dibuja();
        this.pared5.y = -this.pared5.y + 1.4f;
        this.pared5.Dibuja();
        this.piso_com.Dibuja();

        this.f1.Dibuja();
        this.f2.x = -this.f2.x - 38.84f;
        this.f2.Dibuja();
        this.f2.x = -this.f2.x - 38.84f;
        this.f2.Dibuja();
        this.f3.Dibuja();
        this.f4.x = -this.f4.x - 38.84f;
        this.f4.Dibuja();
        this.f4.x = -this.f4.x - 38.84f;
        this.f4.Dibuja();

        this.lin1.Dibuja();
        this.lin2.Dibuja();
        this.lin3.Dibuja();
        this.lin4.Dibuja();

        this.ventana.Dibuja();
        this.b1.x = -this.b1.x - 38.4f;
        this.b1.Dibuja();
        this.b1.x = -this.b1.x - 38.4f;
        this.b1.Dibuja();
        this.b2.y = -this.b2.y + 1.4f;
        this.b2.Dibuja();
        this.b2.y = -this.b2.y + 1.4f;
        this.b2.Dibuja();

        this.marco1.z = -this.marco1.z + .235f;
        this.marco1.Dibuja();
        this.marco1.z = -this.marco1.z + .235f;
        this.marco1.Dibuja();
        this.marco2.Dibuja();
        this.marco3.x = -this.marco3.x - 38.4f;
        this.marco3.Dibuja();
        this.marco3.x = -this.marco3.x - 38.4f;
        this.marco3.Dibuja();
        this.marco4.y = -this.marco4.y + 1.4f;
        this.marco4.Dibuja();
        this.marco4.y = -this.marco4.y + 1.4f;
        this.marco4.Dibuja();

        this.mesa.Dibuja();
        this.pata1.x = -this.pata1.x - 39.9f;
        this.pata1.Dibuja();
        this.pata1.x = -this.pata1.x - 39.9f;
        this.pata1.Dibuja();
        this.pata2.x = -this.pata2.x - 39.9f;
        this.pata2.Dibuja();
        this.pata2.x = -this.pata2.x - 39.9f;
        this.pata2.Dibuja();

        this.silla1.Dibuja();
        this.silla2.Dibuja();
        this.silla3.Dibuja();
        this.silla4.Dibuja();

        this.cu1.Dibuja();
        this.cu2.Dibuja();
        this.cu3.Dibuja();
        this.cu4.Dibuja();
        this.cu5.Dibuja();
        this.mcu1.Dibuja();
        this.mcu2.Dibuja();
        this.mcu3.Dibuja();
        this.mcu4.Dibuja();
        this.mcu5.Dibuja();

        this.mueble.Dibuja();
        this.cajon1.Dibuja();
        this.cajon2.Dibuja();
        this.cajon3.Dibuja();
        this.mm1.Dibuja();
        this.mm2.Dibuja();
        this.mm3.Dibuja();

        this.relojero.Dibuja();
        this.rel1.Dibuja();
        this.rel2.Dibuja();
        this.vidrio.Dibuja();
        this.rel.Dibuja();
        this.reloj.Dibuja();
        this.pen.Dibuja();
        this.pend.Dibuja();
        this.mane1.Dibuja();
        this.mane2.Dibuja();

        this.cort.Dibuja();
        this.cor1.Dibuja();
        this.cor2.Dibuja();
        this.cora1.x = -this.cora1.x - 38.2f;
        this.cora1.Dibuja();
        this.cora1.x = -this.cora1.x - 38.2f;
        this.cora1.Dibuja();
        this.cora2.x = -this.cora2.x - 38.2f;
        this.cora2.Dibuja();
        this.cora2.x = -this.cora2.x - 38.2f;
        this.cora2.Dibuja();
        this.cora3.x = -this.cora3.x - 38.2f;
        this.cora3.Dibuja();
        this.cora3.x = -this.cora3.x - 38.2f;
        this.cora3.Dibuja();
        this.cortina.x = -this.cortina.x - 38.2f;
        this.cortina.Dibuja();
        this.cortina.x = -this.cortina.x - 38.2f;
        this.cortina.Dibuja();

        gl.glEnd();
        gl.glPopMatrix();

    }
}
