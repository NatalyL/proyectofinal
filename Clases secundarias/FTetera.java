/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class FTetera {
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float rx, ry, rz; //rotacion
    float r, g, b;//color
    
    GL gl;
    GLUT glut = new GLUT();

    public FTetera(GL gl, float x, float y, float z, float w, float h, float p, float rx, float ry, float rz, float r, float g, float b) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
        this.r = r;
        this.g = g;
        this.b = b;
        this.gl = gl;

    }

    public void Dibuja() {

        gl.glPushMatrix();
        gl.glColor3f(r, g, b);
        gl.glTranslatef(x, y, z);
        gl.glRotatef(rx, 1, 0, 0);
        gl.glRotatef(ry, 0, 1, 0);
        gl.glRotatef(rz, 0, 0, 1);
        gl.glScalef(w, h, p);

        glut.glutSolidTeapot(1);

        gl.glEnd();

        gl.glPopMatrix();
    }
}
