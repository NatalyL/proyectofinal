/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class Gradas {

//    ArrayList<E_Arbol_2_Esfera> bosquesillo2 = new ArrayList<E_Arbol_2_Esfera>();
    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float rx, ry, rz; //rotacion
    float r, g, b; //color

    FCubo esc1, esc2, esc3, esc4, esc5, esc6, esc7, esc8, esc9, esc10, esc11,
            ba1, ba2, ba3, ba4, ba5, ba6, ba7, ba8, ba9, ba10, ba11;

    public Gradas(GL gl, float x, float y, float z, float w, float h, float p, float rx, float ry, float rz) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
        this.p = p;

        //Escaleras
        this.esc1 = new FCubo(gl, -3.3f, -6.75f, -.16f, .96f, .29f, .04f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.98f, 1f, 1f);
        this.esc2 = new FCubo(gl, -3.3f, -6.17f, -.2f, .96f, .29f, .04f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.98f, 1f, 1f);
        this.esc3 = new FCubo(gl, -3.3f, -5.59f, -.24f, .96f, .29f, .04f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.98f, 1f, 1f);
        this.esc4 = new FCubo(gl, -3.3f, -5.01f, -.28f, .96f, .29f, .04f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.98f, 1f, 1f);
        this.esc5 = new FCubo(gl, -3.3f, -4.43f, -.32f, .96f, .29f, .04f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.98f, 1f, 1f);
        this.esc6 = new FCubo(gl, -3.3f, -3.85f, -.36f, .96f, .29f, .04f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.98f, 1f, 1f);
        this.esc7 = new FCubo(gl, -3.3f, -3.27f, -.4f, .96f, .29f, .04f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.98f, 1f, 1f);
        this.esc8 = new FCubo(gl, -3.3f, -2.69f, -.44f, .96f, .29f, .04f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.98f, 1f, 1f);
        this.esc9 = new FCubo(gl, -3.3f, -2.11f, -.48f, .96f, .29f, .04f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.98f, 1f, 1f);
        this.esc10 = new FCubo(gl, -3.3f, -1.53f, -.52f, .96f, .29f, .04f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.98f, 1f, 1f);
        this.esc11 = new FCubo(gl, -3.3f, -.95f, -.56f, .96f, .29f, .04f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.98f, 1f, 1f);
        
        //Barandilla
        this.ba1 = new FCubo(gl, -4.22f, -5.75f, -.16f, .036f, .9f, .01f, 0, 0, 180, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);
        this.ba2 = new FCubo(gl, -4.22f, -5.17f, -.2f, .036f, .9f, .01f, 0, 0, 180, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);
        this.ba3 = new FCubo(gl, -4.22f, -4.59f, -.24f, .036f, .9f, .01f, 0, 0, 180, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);
        this.ba4 = new FCubo(gl, -4.22f, -4.01f, -.28f, .036f, .9f, .01f, 0, 0, 180, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);
        this.ba5 = new FCubo(gl, -4.22f, -3.43f, -.32f, .036f, .9f, .01f, 0, 0, 180, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);
        this.ba6 = new FCubo(gl, -4.22f, -2.85f, -.36f, .036f, .9f, .01f, 0, 0, 180, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);
        this.ba7 = new FCubo(gl, -4.22f, -2.27f, -.4f, .036f, .9f, .01f, 0, 0, 180, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);
        this.ba8 = new FCubo(gl, -4.22f, -1.69f, -.44f, .036f, .9f, .01f, 0, 0, 180, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);
        this.ba9 = new FCubo(gl, -4.22f, -1.11f, -.48f, .036f, .9f, .01f, 0, 0, 180, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);
        this.ba10 = new FCubo(gl, -4.22f, -0.53f, -.52f, .036f, .9f, .01f, 0, 0, 180, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);
        this.ba11 = new FCubo(gl, -4.22f, .05f, -.56f, .036f, .9f, .01f, 0, 0, 180, 0.95f, 0.95f, 0.95f, 1f, 1f, 1f);
    }

    public void Dibuja() {

        gl.glPushMatrix();

        gl.glTranslated(this.x, this.y, this.z);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);
        gl.glScaled(w, h, p);

        this.esc1.Dibuja();
        this.esc2.Dibuja();
        this.esc3.Dibuja();
        this.esc4.Dibuja();
        this.esc5.Dibuja();
        this.esc6.Dibuja();
        this.esc7.Dibuja();
        this.esc8.Dibuja();
        this.esc9.Dibuja();
        this.esc10.Dibuja();
        this.esc11.Dibuja();
        
        this.ba1.Dibuja();
        this.ba2.Dibuja();
        this.ba3.Dibuja();
        this.ba4.Dibuja();
        this.ba5.Dibuja();
        this.ba6.Dibuja();
        this.ba7.Dibuja();
        this.ba8.Dibuja();
        this.ba9.Dibuja();
        this.ba10.Dibuja();
        this.ba11.Dibuja();

        gl.glEnd();
        gl.glPopMatrix();

    }

    public void gira() {
        rz += 1;
    }
}
