/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class FArmarios {

    GL gl;
    float x, y, z; //traslacion
    float w, h, d; //escalado
    float rx, ry, rz; //rotacion

    FCubo a1, p1, p2, a2, p3, p4, p5, p6;
    FCirculo ma1, ma2, ma3, ma4, ma5, ma6;

    public FArmarios(GL gl, float x, float y, float z, float w, float h, float d, float rx, float ry, float rz) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.d = d;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;

        //1: Bajos
        this.a1 = new FCubo(gl, -14.6f, -1.9f, 1.46f, 11.21f, .3f, 7f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.p1 = new FCubo(gl, -14.5f, -2.f, -1.66f, 11.21f, .167f, 2.9f, 0, 180, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.p2 = new FCubo(gl, -14.5f, -2.f, 4.56f, 11.21f, .167f, 2.9f, 0, 180, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.p5 = new FCubo(gl, -14.5f, -1.72f, -1.66f, 11.21f, .083f, 2.9f, 0, 180, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.p6 = new FCubo(gl, -14.5f, -1.72f, 4.56f, 11.21f, .083f, 2.9f, 0, 180, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.ma1 = new FCirculo(gl, -2.3f, -2f, .56f, .5f, .025f, .36f, 0, 90, 0, 0.8f, 0.8f, 0.8f);
        this.ma2 = new FCirculo(gl, -2.3f, -2f, 2.36f, .5f, .025f, .36f, 0, 90, 0, 0.8f, 0.8f, 0.8f);
        this.ma5 = new FCirculo(gl, -2.3f, -1.72f, -1.66f, .5f, .025f, .36f, 0, 90, 0, 0.8f, 0.8f, 0.8f);
        this.ma6 = new FCirculo(gl, -2.3f, -1.72f, 4.56f, .5f, .025f, .36f, 0, 90, 0, 0.8f, 0.8f, 0.8f);

        //2: Altos
        this.a2 = new FCubo(gl, -14.6f, -1.9f, 1.46f, 11.21f, .3f, 7f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.p3 = new FCubo(gl, -14.5f, -1.9f, -1.66f, 11.21f, .27f, 2.9f, 0, 180, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.p4 = new FCubo(gl, -14.5f, -1.9f, 4.56f, 11.21f, .27f, 2.9f, 0, 180, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.ma3 = new FCirculo(gl, -2.3f, -2.1f, .56f, .5f, .025f, .36f, 0, 90, 0, 0.8f, 0.8f, 0.8f);
        this.ma4 = new FCirculo(gl, -2.3f, -2.1f, 2.36f, .5f, .025f, .36f, 0, 90, 0, 0.8f, 0.8f, 0.8f);
    }

    public void Dibuja1() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);

        gl.glScalef(this.w, this.h, this.d);

        this.a1.Dibuja();
        this.p1.Dibuja();
        this.p2.Dibuja();
        this.p5.Dibuja();
        this.p6.Dibuja();
        this.ma1.Dibuja();
        this.ma2.Dibuja();
        this.ma5.Dibuja();
        this.ma6.Dibuja();

        gl.glPopMatrix();
    }

    public void Dibuja2() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);

        gl.glScalef(this.w, this.h, this.d);

        this.a2.Dibuja();
        this.p3.Dibuja();
        this.p4.Dibuja();
        this.ma3.Dibuja();
        this.ma4.Dibuja();

        gl.glPopMatrix();
    }
}
