/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class PrimerPiso {
    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float ry; //rotacion

    AGaraje garaje;
    ACocina cocina;
    AComedor comedor;
    ASala sala;
    AEntrada entrada;
    AEstudio estudio;
    ADormitorio dormitorio;
    APasillo pasillo;

    public PrimerPiso(GL gl, float x, float y, float z, float w, float h, float p, float ry) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.ry = ry;

        //Garaje
        this.garaje = new AGaraje(gl, 0, 0.1f, 0, 1, 1, 1, 0);

        //Cocina
        this.cocina = new ACocina (gl, 0, 0.1f, 0, 1, 1, 1, 0);
        
        //Comedor
        this.comedor = new AComedor (gl, 0, 0.1f, 0, 1, 1, 1, 0);
        
        //Sala
        this.sala = new ASala (gl, 0, 0.1f, 0, 1, 1, 1, 0);
                
        //Entrada
        this.entrada = new AEntrada (gl, 0, 0.1f, 0, 1, 1, 1, 0);
                
        //Estudio
        this.estudio = new AEstudio (gl, 0, 0.1f, 0, 1, 1, 1, 0);
                
        //Dormitorio
        this.dormitorio = new ADormitorio (gl, 0, 0.1f, 0, 1, 1, 1, 0);
                
        //Pasillo
        this.pasillo = new APasillo (gl, 0, 0.1f, 0, 1, 1, 1, 0);
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glScalef(this.w, this.h, this.p);

        this.garaje.Dibuja();
        this.cocina.Dibuja();
        this.comedor.Dibuja();
        this.entrada.Dibuja();
        this.sala.Dibuja();
        this.estudio.Dibuja();
        this.dormitorio.Dibuja();
        this.pasillo.Dibuja();
                
        gl.glEnd();
        gl.glPopMatrix();

    }
}
