/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class AGaraje {

    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float ry; //rotacion

    FCubo pared1, pared2, pared3, pared4, pared5, m1, m2, m3, m4;
    FCuadrado piso_g, puerta, techo_g;

    public AGaraje(GL gl, float x, float y, float z, float w, float h, float p, float ry) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.ry = ry;

        //Paredes y suelo
        this.pared1 = new FCubo(gl, -75.1f, .7f, .247f, .3f, 1.57f, .147f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 1f, 1f, 0.8f);//iz
        this.pared2 = new FCubo(gl, -55.57f, .7f, .107f, 19.8f, 1.57f, .0076f, 0, 0, 0, 1f, 1f, 0.8f, 0.7f, 0.7f, 0.7f);//up
        this.pared3 = new FCubo(gl, -36, .7f, .247f, .3f, 1.57f, .147f, 0, 0, 0, 1f, 1f, 0.8f, 0.7f, 0.7f, 0.7f);//der
        this.pared4 = new FCubo(gl, -72.61f, .7f, .387f, 2.81f, 1.57f, .0076f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 1f, 1f, 0.8f);//down.x
        this.pared5 = new FCubo(gl, -55.61f, 1.94f, .387f, 19.81f, .33f, .0076f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 1f, 1f, 0.8f);//down.y
        this.piso_g = new FCuadrado(gl, -55.5f, -.87f, 0.247f, 19.74f, .147f, 90, 0, 0, 0.87f, 0.87f, 0.86f);
        this.puerta = new FCuadrado(gl, -55.61f, .7f, 0.384f, 19.8f, 1.57f, 0, 0, 0, 0.9f, 0.85f, 0.76f);
        this.techo_g = new FCuadrado(gl, -55.5f, 2.3f, 0.247f, 19.74f, .147f, 90, 0, 0, 1f, 1f, 0.8f);

        //Marco
        this.m1 = new FCubo(gl, -55.61f, 1.57f, .384f, 15.f, .06f, .0038f, 0, 0, 0, 0.85f, 0.8f, 0.6f, 0.85f, 0.8f, 0.6f);//y
        this.m2 = new FCubo(gl, -55.61f, .5f, .384f, 15.f, .06f, .0038f, 0, 0, 0, 0.85f, 0.8f, 0.6f, 0.85f, 0.8f, 0.6f);//y
        this.m3 = new FCubo(gl, -69.61f, .5f, .384f, .3f, 1.27f, .0038f, 0, 0, 0, 0.85f, 0.8f, 0.6f, 0.85f, 0.8f, 0.6f);//x
        this.m4 = new FCubo(gl, -55.61f, .5f, .384f, .3f, 1.27f, .0038f, 0, 0, 0, 0.85f, 0.8f, 0.6f, 0.85f, 0.8f, 0.6f);//x
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glScalef(this.w, this.h, this.p);

        this.pared1.Dibuja();
        this.pared2.Dibuja();
        this.pared3.Dibuja();
        this.pared4.x = -this.pared4.x - 111.19f;
        this.pared4.Dibuja();
        this.pared4.x = -this.pared4.x - 111.19f;
        this.pared4.Dibuja();
        this.pared5.Dibuja();
        this.piso_g.Dibuja();
        this.puerta.Dibuja();
        this.techo_g.Dibuja();

        this.m1.y = -this.m1.y + .74f;
        this.m1.Dibuja();
        this.m1.y = -this.m1.y + .74f;
        this.m1.Dibuja();
        this.m2.Dibuja();
        this.m3.x = -this.m3.x - 111.3f;
        this.m3.Dibuja();
        this.m3.x = -this.m3.x - 111.3f;
        this.m3.Dibuja();
        this.m4.Dibuja();

        gl.glEnd();
        gl.glPopMatrix();

    }
}
