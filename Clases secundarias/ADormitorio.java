/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class ADormitorio {

    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float ry; //rotacion

    FCubo pared1, pared2, pared3, pared4, pared5, pared6, marco3, marco4, b1, b2;
    FCuadrado piso_d, techo_d;
    FVidrio ventana;

    public ADormitorio(GL gl, float x, float y, float z, float w, float h, float p, float ry) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.ry = ry;

        //Paredes y suelo
        this.pared1 = new FCubo(gl, 25.9f, .7f, .214f, .3f, 1.57f, .18f, 0, 0, 0, 0.95f, 0.9f, 0.75f, 1f, 1f, 0.8f);//iz
        this.pared2 = new FCubo(gl, 46.43f, .7f, .028f, 10.3f, 1.57f, .0076f, 0, 0, 0, 0.97f, 1f, 0.82f, 0.95f, 0.9f, 0.75f);//up.x
        this.pared6 = new FCubo(gl, 31.13f, 1.94f, .028f, 5.3f, .33f, .0076f, 0, 0, 0, 0.97f, 1f, 0.82f, 0.95f, 0.9f, 0.75f);//up.y
        this.pared3 = new FCubo(gl, 56.6f, .7f, .214f, .3f, 1.57f, .18f, 0, 0, 0, 1f, 1f, 0.8f, 0.95f, 0.9f, 0.75f);//der
        this.pared4 = new FCubo(gl, 29.79f, .7f, .387f, 3.99f, 1.57f, .0076f, 0, 0, 0, 0.95f, 0.9f, 0.75f, 1f, 1f, 0.8f);//down.x
        this.pared5 = new FCubo(gl, 42.99f, 1.94f, .387f, 13.8f, .33f, .0076f, 0, 0, 0, 0.95f, 0.9f, 0.75f, 1f, 1f, 0.8f);//down.y
        this.piso_d = new FCuadrado(gl, 41.2f, -.87f, 0.205f, 15.54f, .176f, 90, 0, 0, 0.8f, 0.85f, 0.7f);
        this.techo_d = new FCuadrado(gl, 41.2f, 2.3f, 0.189f, 15.54f, .206f, 90, 0, 0, 1f, 1f, 0.8f);

        //Ventana
        this.ventana = new FVidrio(gl, 42.99f, 0.7f, .387f, 13.8f, 1.57f, 0, 0, 0);
        this.b1 = new FCubo(gl, 41.15f, .69f, .387f, .15f, 0.9f, .007f, 0, 0, 0, 1f, 1f, 1f, 0.7f, 0.5f, 0.3f);//down.x

        //Marcos ventanas
        this.marco3 = new FCubo(gl, 33.79f, .69f, .387f, .3f, 0.9f, .0079f, 0, 0, 0, 1f, 1f, 1f, 0.7f, 0.5f, 0.3f);//down.x
        this.marco4 = new FCubo(gl, 41.15f, 1.59f, .387f, 7.7f, .06f, .0079f, 0, 0, 0, 1f, 1f, 1f, 0.7f, 0.5f, 0.3f);//down.y
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glScalef(this.w, this.h, this.p);

        this.pared1.Dibuja();
        this.pared2.Dibuja();
        this.pared6.Dibuja();
        this.pared3.Dibuja();
        this.pared4.x = -this.pared4.x + 82.6f;
        this.pared4.Dibuja();
        this.pared4.x = -this.pared4.x + 82.6f;
        this.pared4.Dibuja();
        this.pared5.y = -this.pared5.y + 1.4f;
        this.pared5.Dibuja();
        this.pared5.y = -this.pared5.y + 1.4f;
        this.pared5.Dibuja();
        this.piso_d.Dibuja();

        this.ventana.Dibuja();
        this.b1.Dibuja();

        this.marco3.x = -this.marco3.x + 82.35f;
        this.marco3.Dibuja();
        this.marco3.x = -this.marco3.x + 82.35f;
        this.marco3.Dibuja();
        this.marco4.y = -this.marco4.y + 1.4f;
        this.marco4.Dibuja();
        this.marco4.y = -this.marco4.y + 1.4f;
        this.marco4.Dibuja();
        
        this.techo_d.Dibuja();

        gl.glEnd();
        gl.glPopMatrix();

    }
}
