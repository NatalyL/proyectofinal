/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class Nave {

    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float rx, ry, rz; //rotacion

    FEsfera vidrio, nave, dis1, dis2, dis3, dis4, pro, luz;
    FCilindro prop, di1, di2, di3, di4, foco;
    FCirculo pr;

    public Nave(GL gl, float x, float y, float z, float w, float h, float p, float rx, float ry, float rz) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;

        //Cuerpo
        this.vidrio = new FEsfera(gl, -4.5f, .64f, 0, 1.f, 1.1f, 1.f, 0, 0, 0, 1f, 0.9f, 0.7f);
        this.nave = new FEsfera(gl, -4.5f, .14f, 0, 2.f, .5f, 2.f, 0, 0, 0, 0.3f, 0.3f, 0.3f);
        this.dis1 = new FEsfera(gl, -4.5f, .14f, 0, .2f, .51f, 2.1f, 0, 0, 0, 0.9f, 0.9f, 0.2f);
        this.dis2 = new FEsfera(gl, -4.5f, .14f, 0, .2f, .51f, 2.1f, 0, 45, 0, 0.9f, 0.9f, 0.2f);
        this.dis3 = new FEsfera(gl, -4.5f, .14f, 0, .2f, .51f, 2.1f, 0, 90, 0, 0.9f, 0.9f, 0.2f);
        this.dis4 = new FEsfera(gl, -4.5f, .14f, 0, .2f, .51f, 2.1f, 0, 135, 0, 0.9f, 0.9f, 0.2f);

        //Propulsores
        this.prop = new FCilindro(gl, -6.2f, .75f, 0, 1.2f, 1.25f, 1.5f, 90, 0, 0, 0.4f, 0.4f, 0.4f, 0.3f, 0.3f);
        this.pro = new FEsfera(gl, -6.2f, .75f, 1.35f, .34f, .34f, .34f, 0, 0, 0, 0.6f, 0.6f, 0.6f);
        this.pr = new FCirculo(gl, -6.2f, .75f, 0f, .36f, .36f, .36f, 0, 0, 0, 0.4f, 0.4f, 0.4f);
        this.di1 = new FCilindro(gl, -6.2f, .75f, .3f, .2f, 1.28f, 1.f, 90, 0, 0, 0.2f, 0.2f, 0.2f, 0.3f, 0.3f);
        this.di2 = new FCilindro(gl, -6.2f, .75f, .3f, .2f, 1.27f, 1.f, 90, 0, 45, 0.2f, 0.2f, 0.2f, 0.3f, 0.3f);
        this.di3 = new FCilindro(gl, -6.2f, .75f, .3f, .2f, 1.23f, 1.f, 90, 0, 90, 0.2f, 0.2f, 0.2f, 0.3f, 0.3f);
        this.di4 = new FCilindro(gl, -6.2f, .75f, .3f, .2f, 1.27f, 1.f, 90, 0, 135, 0.2f, 0.2f, 0.2f, 0.3f, 0.3f);

        //Luces
        this.foco = new FCilindro(gl, -5.1f, .45f, -1.6f, 1.2f, 1.25f, .3f, 90, 0, 0, 0.4f, 0.4f, 0.4f, 0.07f, 0.07f);
        this.luz = new FEsfera(gl, -5.1f, .45f, -1.6f, .074f, .074f, .074f, 0, 0, 0, 0.9f, 0.9f, 0.2f);
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glScalef(this.w, this.h, this.p);

        this.vidrio.DibujaTr();
        this.nave.Dibuja();
        this.dis1.Dibuja();
        this.dis2.Dibuja();
        this.dis3.Dibuja();
        this.dis4.Dibuja();

        this.prop.x = -this.prop.x - 8.95f;
        this.prop.Dibuja();
        this.prop.x = -this.prop.x - 8.95f;
        this.prop.Dibuja();
        this.pro.x = -this.pro.x - 8.95f;
        this.pro.Dibuja();
        this.pro.x = -this.pro.x - 8.95f;
        this.pro.Dibuja();
        this.di1.x = -this.di1.x - 8.95f;
        this.di1.Dibuja();
        this.di1.x = -this.di1.x - 8.95f;
        this.di1.Dibuja();
        this.di2.x = -this.di2.x - 8.95f;
        this.di2.Dibuja();
        this.di2.x = -this.di2.x - 8.95f;
        this.di2.Dibuja();
        this.di3.x = -this.di3.x - 8.95f;
        this.di3.Dibuja();
        this.di3.x = -this.di3.x - 8.95f;
        this.di3.Dibuja();
        this.di4.x = -this.di4.x - 8.95f;
        this.di4.Dibuja();
        this.di4.x = -this.di4.x - 8.95f;
        this.di4.Dibuja();

        pr.x = -pr.x - 8.95f;
        pr.Dibuja();
        pr.x = -pr.x - 8.95f;
        pr.Dibuja();

        this.foco.x = -this.foco.x - 8.95f;
        this.foco.Dibuja();
        this.foco.x = -this.foco.x - 8.95f;
        this.foco.Dibuja();
        this.luz.x = -this.luz.x - 8.95f;
        this.luz.Dibuja();
        this.luz.x = -this.luz.x - 8.95f;
        this.luz.Dibuja();

        gl.glEnd();
        gl.glPopMatrix();

    }

    public void movimiento_nave() {
        this.x += .3;
        this.y += .041;
        if (this.x > 25 && this.y > 7) {
            this.x = 25;
            this.y = 7;
            if (this.x == 25) {
//            this.y += .015;
                this.z -= .2;
//            this.x -= .31;
                if (this.z <= -22) {
                    this.z = -22;
                }
            }
        }
        if (this.z == -22) {
            this.x -= .5;
            this.y -= .06;
            if (this.x <= -24) {
                this.x = -24;
            }
            if (this.y <= 1) {
                this.y = 1;
            }
        }
    }
}
