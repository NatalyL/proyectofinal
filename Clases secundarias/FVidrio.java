/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class FVidrio {

    GL gl;
    float x, y, z; //traslacion
    float w, h; //escalado
    float rx, ry, rz; //rotacion

    public FVidrio(GL gl, float x, float y, float z, float w, float h, float rx, float ry, float rz) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
    }

    public void Dibuja() {

        gl.glPushMatrix();

        gl.glTranslated(this.x, this.y, this.z);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);
        gl.glScaled(w, h, 0);

        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        gl.glEnable(GL.GL_BLEND);
        gl.glColor4f(0.5f, 0.8f, 0.9f, 0.5f);
        gl.glBegin(gl.GL_QUADS);

        gl.glVertex2d(1, 1);
        gl.glVertex2d(1, -1);
        gl.glVertex2d(-1, -1);
        gl.glVertex2d(-1, 1);

        gl.glEnd();
        gl.glPopMatrix();

    }
    
    public void Dibuja1() {

        gl.glPushMatrix();

        gl.glTranslated(this.x, this.y, this.z);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);
        gl.glScaled(w, h, 0);

        gl.glColor3f(0.5f, 0.8f, 0.9f);
        gl.glBegin(gl.GL_QUADS);

        gl.glVertex2d(1, 1);
        gl.glVertex2d(1, -1);
        gl.glVertex2d(-1, -1);
        gl.glVertex2d(-1, 1);

        gl.glEnd();
        gl.glPopMatrix();

    }
}
