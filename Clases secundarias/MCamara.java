/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author NATALY MICHELLE
 */
public class MCamara implements KeyListener {

    public void keyTyped(KeyEvent ke) {

        if (ke.getKeyChar() == '1') {
            ProyectoFinal.num_cam = 1;
            ProyectoFinal.camx = 0;
            ProyectoFinal.camy = 1;
            ProyectoFinal.camz = 20;
        }

        if (ke.getKeyChar() == '2') {
            ProyectoFinal.num_cam = 2;
        }

        if (ke.getKeyChar() == '3') {
            ProyectoFinal.num_cam = 3;
        }

        if (ke.getKeyChar() == '4') {
            ProyectoFinal.num_cam = 4;
        }

        if (ke.getKeyChar() == '5') {
            ProyectoFinal.num_cam = 5;
        }
    }

    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            ProyectoFinal.camx += 0.5;
        }

        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            ProyectoFinal.camx -= 0.5;
        }

        if (ke.getKeyCode() == KeyEvent.VK_UP) {
            ProyectoFinal.camy += 0.5;
        }

        if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            ProyectoFinal.camy -= 0.5;
        }

        if (ke.getKeyCode() == KeyEvent.VK_X) {
            ProyectoFinal.camz -= 0.5;
        }

        if (ke.getKeyCode() == KeyEvent.VK_Z) {
            ProyectoFinal.camz += 0.5;
        }
        
        if (ke.getKeyCode() == KeyEvent.VK_D) {
            ProyectoFinal.vistx += 0.5;
        }

        if (ke.getKeyCode() == KeyEvent.VK_A) {
            ProyectoFinal.vistx -= 0.5;
        }
    }

    public void keyReleased(KeyEvent ke) {

    }

}
