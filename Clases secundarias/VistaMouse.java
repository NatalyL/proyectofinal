/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 *
 * @author NATALY MICHELLE
 */
public class VistaMouse implements MouseListener, MouseMotionListener {

    public void mouseClicked(MouseEvent me) {
        if (me.isMetaDown()) {
            ProyectoFinal.vistz = ProyectoFinal.vistz + 1;
        } else {
            ProyectoFinal.vistz = ProyectoFinal.vistz - 1;
        }
    }

    public void mousePressed(MouseEvent me) {

    }

    public void mouseReleased(MouseEvent me) {

    }

    public void mouseEntered(MouseEvent me) {

    }

    public void mouseExited(MouseEvent me) {

    }

    public void mouseDragged(MouseEvent me) {

    }

    public void mouseMoved(MouseEvent me) {
        int x = me.getX();
        int y = me.getY();

        if (x < 250) {
            ProyectoFinal.vistx = ProyectoFinal.vistx - .3;
        }
        
        if (x > 350) {
            ProyectoFinal.vistx = ProyectoFinal.vistx + .3;
        }
        
        if (x < 250) {
            ProyectoFinal.vistx = ProyectoFinal.vistx - .3;
        }
        
        if (x > 350) {
            ProyectoFinal.vistx = ProyectoFinal.vistx + .3;
        }

        if (y < 250) {
            ProyectoFinal.visty = ProyectoFinal.visty + .3;
        }
        
        if (y > 350) {
            ProyectoFinal.visty = ProyectoFinal.visty - .3;
        }
    }

}
