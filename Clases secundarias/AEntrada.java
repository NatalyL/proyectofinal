/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class AEntrada {

    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float ry; //rotacion

    FCubo pared1, pared2, pared3, pared4, pared5, pared6, pared7, pared8, marco1, marco2, marco3, marco4,
            puerta1, puerta2, piso, alfombra, cu1, cu2, cu3, cu4, cu5, mueble, cajon1, cajon2, cajon3, mm1, mm2,
            mcu1, mm3, mcu2, mcu3, mcu4, mcu5, lin1, lin2, f1, f2, f3, f4, f5, f6, f7;
    FCuadrado piso_e, pared_down;
    FTriangulo pared;
    Gradas gradas;
    FEsfera m1, m2;

    public AEntrada(GL gl, float x, float y, float z, float w, float h, float p, float ry) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.ry = ry;

        //Paredes y suelo
        this.pared1 = new FCubo(gl, -3.2f, .7f, -.01f, .3f, 1.57f, .090f, 0, 0, 0, 1f, 0.9f, 0.7f, 1f, 0.97f, 0.75f);//iz.x
        this.pared8 = new FCubo(gl, -3.2f, 1.94f, .117f, .3f, 0.33f, .06f, 0, 0, 0, 1f, 0.9f, 0.7f, 1f, 0.97f, 0.75f);//iz.y
        this.pared2 = new FCubo(gl, .9f, .7f, -.107f, 3.81f, 1.57f, .0076f, 0, 0, 0, 1f, 0.97f, 0.75f, 1f, 0.9f, 0.7f);//up.x
        this.pared6 = new FCubo(gl, 10.2f, 1.94f, -.107f, 6.2f, .33f, .0076f, 0, 0, 0, 1f, 0.97f, 0.75f, 1f, 0.9f, 0.7f);//up.y
        this.pared3 = new FCubo(gl, 25.5f, .7f, .097f, .3f, 1.57f, .092f, 0, 180, 0, 1f, 0.9f, 0.7f, 0.97f, 1f, 0.82f);//der
        this.pared4 = new FCubo(gl, 22f, .7f, .193f, 3.81f, 1.57f, .0076f, 0, 0, 0, 1f, 0.9f, 0.7f, 1f, 1f, 0.8f);//down.x.1
        this.pared7 = new FCubo(gl, 16.2f, .7f, .193f, 3.81f, 1.57f, .0076f, 0, 0, 0, 1f, 0.9f, 0.7f, 1f, 1f, 0.8f);//down.x.2
        this.pared5 = new FCubo(gl, 11.3f, 1.94f, .193f, 14.6f, .33f, .0076f, 0, 0, 0, 1f, 0.9f, 0.7f, 1f, 1f, 0.8f);//down.y
        this.lin1 = new FCubo(gl, 25.5f, .7f, .192f, .3f, 1.57f, .0076f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//iz.d
        this.lin2 = new FCubo(gl, 16, .7f, -.107f, .3f, 1.57f, .0078f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//iz.u
        this.piso_e = new FCuadrado(gl, 11.1f, -.87f, 0.04f, 14.45f, .147f, 90, 0, 0, 0.92f, 0.85f, 0.6f);

        //Filo inferior
        this.f1 = new FCubo(gl, -3.2f, -.82f, -.01f, .5f, .06f, .090f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//iz.x
        this.f2 = new FCubo(gl, .9f, -.82f, -.107f, 3.81f, .06f, .0078f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//up.x
        this.f3 = new FCubo(gl, 25.5f, -.82f, .097f, .5f, .06f, .092f, 0, 180, 0, 1f, 1f, 1f, 1f, 1f, 1f);//der
        this.f6 = new FCubo(gl, 16.01f, -.82f, .084f, .5f, .06f, .079f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//der.gradas
        this.f7 = new FCubo(gl, 16.01f, -.82f, -.087f, .5f, .06f, .012f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//der.gradas.p
        this.f4 = new FCubo(gl, 22f, -.82f, .19f, 3.8f, .06f, .0076f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//down.x.1
        this.f5 = new FCubo(gl, 16.2f, -.82f, .19f, 3.8f, .06f, .0076f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//down.x.2

        //Marcos
        this.marco1 = new FCubo(gl, 16, .38f, .005f, .45f, 1.27f, .0019f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//der.x
        this.marco2 = new FCubo(gl, 16, 1.59f, -.035f, .4f, .059f, .038f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//der.y

        //Puerta
        this.marco3 = new FCubo(gl, 12.35f, .38f, .193f, .3f, 1.27f, .0079f, 0, 0, 0, 1f, 1f, 1f, 0.7f, 0.5f, 0.3f);//down.x
        this.marco4 = new FCubo(gl, 8.15f, 1.59f, .193f, 4.3f, .06f, .0079f, 0, 0, 0, 1f, 1f, 1f, 0.7f, 0.5f, 0.3f);//down.y
        this.puerta1 = new FCubo(gl, 12.25f, .35f, .153f, .42f, 1.22f, .038f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.7f, 0.5f, 0.3f);//down
        this.puerta2 = new FCubo(gl, 16, .35f, -.035f, .42f, 1.22f, .038f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.7f, 0.5f, 0.3f);//der

        //Gradas
        this.pared = new FTriangulo(gl, 16, 0.7f, .08f, .075f, 1.57f, 0, 270, 0, 1f, 0.9f, 0.7f);
        this.pared_down = new FCuadrado(gl, 20.8f, 0.7f, .005f, 4.9f, 1.57f, 0, 0, 0, 1f, 0.9f, 0.7f);
        this.gradas = new Gradas(gl, 36.5f, 1.94f, .193f, 4.8f, .4f, .32f, 0, 0, 0);

        //Piso
        this.piso = new FCubo(gl, 8.15f, -.88f, .225f, 4.3f, .06f, .035f, 0, 0, 0, .9f, .59f, .59f, .9f, .59f, .59f);

        //Manijas
        this.m1 = new FEsfera(gl, 15.05f, .35f, -.058f, .4f, .05f, .004f, 0, 0, 0, 0.9f, 1.f, 0.1f);
        this.m2 = new FEsfera(gl, 11.3f, .35f, .13f, .4f, .05f, .004f, 0, 0, 0, 0.9f, 1.f, 0.1f);//entrada

        //Detalles
        //Alfombra y cuadros
        this.alfombra = new FCubo(gl, 8.15f, -.88f, .035f, 4.3f, .06f, .075f, 0, 0, 0, 1f, .7f, .4f, 1f, .7f, .4f);
        this.cu1 = new FCubo(gl, 25.f, .5f, .133f, .21f, .225f, .007f, 0, 0, 0, 0.8f, 0.5f, 1f, 0.8f, 0.5f, 1f);
        this.mcu1 = new FCubo(gl, 25.1f, .5f, .133f, .21f, .255f, .009f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu2 = new FCubo(gl, 25.f, 1.2f, .133f, .21f, .225f, .007f, 0, 0, 0, 0.98f, 0.65f, 1f, 0.98f, 0.65f, 1f);
        this.mcu2 = new FCubo(gl, 25.1f, 1.2f, .133f, .21f, .255f, .009f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu3 = new FCubo(gl, 25.f, 1f, .163f, .21f, .225f, .007f, 0, 0, 0, 0.8f, 0.85f, 1f, 0.8f, 0.85f, 1f);
        this.mcu3 = new FCubo(gl, 25.1f, 1f, .163f, .21f, .255f, .009f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu4 = new FCubo(gl, .3f, .95f, .18f, 1.01f, .225f, .002f, 0, 0, 0, 0.8f, 0.85f, 1f, 0.8f, 0.85f, 1f);
        this.mcu4 = new FCubo(gl, .3f, .95f, .181f, 1.18f, .255f, .002f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cu5 = new FCubo(gl, -2.6f, 1f, .02f, .21f, .225f, .007f, 0, 0, 0, 0.95f, 0.99f, 0.55f, 0.9f, 1f, 0.5f);
        this.mcu5 = new FCubo(gl, -2.7f, 1f, .02f, .21f, .255f, .009f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);

        //Mueble
        this.mueble = new FCubo(gl, -1f, 0f, -.070f, 1.81f, .87f, .038f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.cajon1 = new FCubo(gl, -.9f, 0.6f, -.070f, 1.81f, .174f, .034f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.7f, 0.5f, 0.3f);
        this.cajon2 = new FCubo(gl, -.9f, -0.6f, -.070f, 1.81f, .174f, .034f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.7f, 0.5f, 0.3f);
        this.cajon3 = new FCubo(gl, -.9f, 0f, -.070f, 1.81f, .174f, .034f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.7f, 0.5f, 0.3f);
        this.mm1 = new FCubo(gl, -.89f, 0.6f, -.070f, 1.81f, .017f, .003f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.75f, 0.75f, 0.75f);
        this.mm2 = new FCubo(gl, -.89f, -0.6f, -.070f, 1.81f, .017f, .003f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.75f, 0.75f, 0.75f);
        this.mm3 = new FCubo(gl, -.89f, 0f, -.070f, 1.81f, .017f, .003f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.75f, 0.75f, 0.75f);
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glScalef(this.w, this.h, this.p);

        this.pared1.Dibuja();
        this.pared8.Dibuja();
        this.pared2.Dibuja();
        this.pared6.Dibuja();
        this.pared3.Dibuja();
        this.pared4.x = -this.pared4.x + 22.34f;
        this.pared4.Dibuja();
        this.pared4.x = -this.pared4.x + 22.34f;
        this.pared4.Dibuja();
        this.pared7.Dibuja();
        this.pared5.Dibuja();
        this.piso_e.Dibuja();

        this.f1.Dibuja();
        this.f2.Dibuja();
        this.f3.Dibuja();
        this.f4.x = -this.f4.x + 22.34f;
        this.f4.Dibuja();
        this.f4.x = -this.f4.x + 22.34f;
        this.f4.Dibuja();
        this.f5.Dibuja();
        this.f6.Dibuja();
        this.f7.Dibuja();

        this.lin1.Dibuja();
        this.lin2.Dibuja();

        this.marco1.z = -this.marco1.z - .069f;
        this.marco1.Dibuja();
        this.marco1.z = -this.marco1.z - .069f;
        this.marco1.Dibuja();
        this.marco2.Dibuja();

        this.marco3.x = -this.marco3.x + 16.4f;
        this.marco3.Dibuja();
        this.marco3.x = -this.marco3.x + 16.4f;
        this.marco3.Dibuja();
        this.marco4.Dibuja();

        this.pared.Dibuja();
        this.pared_down.Dibuja();
        this.puerta1.Dibuja();
        this.puerta2.Dibuja();
        this.piso.Dibuja();
        this.gradas.Dibuja();

        this.m1.Dibuja();
        this.m2.Dibuja();

        this.alfombra.Dibuja();
        this.cu1.Dibuja();
        this.cu2.Dibuja();
        this.cu3.Dibuja();
        this.cu4.Dibuja();
        this.cu5.Dibuja();
        this.mcu1.Dibuja();
        this.mcu2.Dibuja();
        this.mcu3.Dibuja();
        this.mcu4.Dibuja();
        this.mcu5.Dibuja();
        this.mueble.Dibuja();
        this.cajon1.Dibuja();
        this.cajon2.Dibuja();
        this.cajon3.Dibuja();
        this.mm1.Dibuja();
        this.mm2.Dibuja();
        this.mm3.Dibuja();

        gl.glEnd();
        gl.glPopMatrix();

    }
}
