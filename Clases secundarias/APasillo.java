/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class APasillo {

    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float ry; //rotacion

    FCubo pared1, pared2, pared3, pared4, pared5, pared6, marco1, marco2, marco3, marco4, marco5, marco6, puerta1, puerta2;
    FCuadrado piso_p;

    public APasillo(GL gl, float x, float y, float z, float w, float h, float p, float ry) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.ry = ry;

        //Paredes y suelo
        this.pared1 = new FCubo(gl, 20.8f, .7f, -.087f, 4.9f, 1.57f, .012f, 0, 0, 0, 0.97f, 1f, 0.82f, 1f, 0.9f, 0.7f);//iz.x
        this.pared5 = new FCubo(gl, 20.8f, 1.94f, -.037f, 4.9f, .33f, .042f, 0, 0, 0, 0.97f, 1f, 0.82f, 1f, 0.9f, 0.7f);//iz.y
        this.pared2 = new FCubo(gl, 53.23f, .7f, -.107f, 3.1f, 1.57f, .0076f, 0, 180, 0, 0.97f, 1f, 0.82f, 0.8f, 0.4f, 0.3f);//up.x
        this.pared6 = new FCubo(gl, 46.43f, 1.94f, -.107f, 5.3f, .33f, .0076f, 0, 180, 0, 0.97f, 1f, 0.82f, 0.8f, 0.4f, 0.3f);//up.y
        this.pared3 = new FCubo(gl, 56.6f, .7f, -.035f, .3f, 1.57f, .069f, 0, 0, 0, 1f, 1f, 0.8f, 0.97f, 1f, 0.82f);//der
        this.pared4 = new FCubo(gl, 27.19f, .7f, .028f, 1.3f, 1.57f, .0076f, 0, 0, 0, 0.97f, 1f, 0.82f, 0.95f, 0.9f, 0.75f);//down
        this.piso_p = new FCuadrado(gl, 41.04f, -.87f, -0.041f, 15.62f, .07f, 90, 0, 0, 1f, 1f, 0.85f);

        //Marcos 
        this.marco1 = new FCubo(gl, 50.25f, .38f, -.107f, .3f, 1.27f, .0079f, 0, 0, 0, 1f, 0.8f, 0.6f, 1f, 1f, 1f);//up.x
        this.marco2 = new FCubo(gl, 46.25f, 1.59f, -.107f, 3.72f, .06f, .0079f, 0, 0, 0, 1f, 0.8f, 0.6f, 1f, 1f, 1f);//up.y

        this.marco3 = new FCubo(gl, 36.35f, .38f, .028f, .3f, 1.27f, .0079f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//down.x
        this.marco4 = new FCubo(gl, 32.35f, 1.59f, .028f, 4.1f, .06f, .0079f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//down.y

        this.marco5 = new FCubo(gl, 25.5f, .38f, .005f, .45f, 1.27f, .0019f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//iz.x
        this.marco6 = new FCubo(gl, 25.5f, 1.59f, -.035f, .4f, .059f, .038f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//iz.y

        //Puertas 
        this.puerta1 = new FCubo(gl, 32.35f, .35f, .028f, 4.1f, 1.22f, .0074f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.7f, 0.5f, 0.3f);//down
        this.puerta2 = new FCubo(gl, 46.25f, .35f, -.107f, 3.72f, 1.22f, .0074f, 0, 0, 0, 0.7f, 0.5f, 0.3f, 0.7f, 0.5f, 0.3f);//up
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glScalef(this.w, this.h, this.p);

        this.pared1.Dibuja();
        this.pared2.Dibuja();
        this.pared6.Dibuja();
        this.pared3.Dibuja();
        this.pared4.Dibuja();
        this.pared5.Dibuja();
        this.piso_p.Dibuja();

        this.marco1.x = -this.marco1.x + 93.1f;
        this.marco1.Dibuja();
        this.marco1.x = -this.marco1.x + 93.1f;
        this.marco1.Dibuja();
        this.marco2.Dibuja();

        this.marco3.x = -this.marco3.x + 64.7f;
        this.marco3.Dibuja();
        this.marco3.x = -this.marco3.x + 64.7f;
        this.marco3.Dibuja();
        this.marco4.Dibuja();

        this.marco5.z = -this.marco5.z - .069f;
        this.marco5.Dibuja();
        this.marco5.z = -this.marco5.z - .069f;
        this.marco5.Dibuja();
        this.marco6.Dibuja();
        
        this.puerta1.Dibuja();
        this.puerta2.Dibuja();

        gl.glEnd();
        gl.glPopMatrix();

    }
}
