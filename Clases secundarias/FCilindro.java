/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

/**
 *
 * @author NATALY MICHELLE
 */
public class FCilindro {

    float x, y, z; //traslacion
    float w, h, p; //escalado
    float rx, ry, rz; //rotacion
    float r, g, b;//color

    double rs, ri;//radio superior e inferior
    GL gl;
    GLUT glut = new GLUT();

    public FCilindro(GL gl, float x, float y, float z, float w, float h, float d, float rx, float ry, float rz, float r, float g, float b, double rs, double ri) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = d;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
        this.r = r;
        this.g = g;
        this.b = b;
        this.rs = rs;
        this.ri = ri;
    }

    public void Dibuja() {
        GLU glu = new GLU();
        gl.glPushMatrix();
        gl.glColor3f(this.r, this.g, this.b);
        gl.glTranslatef(this.x, this.y, this.z);

        gl.glRotatef(-90, 1, 0, 0);

        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);
        gl.glScalef(this.w, this.h, this.p);

        GLUquadric cilindro = glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(cilindro, GLU.GLU_FILL);
        glu.gluCylinder(cilindro, ri, rs, 1, 9, 9);

        gl.glEnd();

        gl.glPopMatrix();
    }
    
    public void DibujaL() {

        gl.glPushMatrix();
        gl.glColor3f(r, g, b);
        gl.glTranslatef(x, y, z);
        gl.glRotatef(rx, 1, 0, 0);
        gl.glRotatef(ry, 0, 1, 0);
        gl.glRotatef(rz, 0, 0, 1);
        gl.glScalef(w, h, p);

        glut.glutWireCylinder(rs, 2, 7, 7);

        gl.glEnd();

        gl.glPopMatrix();
    }
    
    public void DibujaTr() {
        GLU glu = new GLU();
        gl.glPushMatrix();
        gl.glColor4f(0.5f, 0.8f, 0.9f, 0.5f);
        gl.glTranslatef(this.x, this.y, this.z);

        gl.glRotatef(-90, 1, 0, 0);

        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);
        gl.glScalef(this.w, this.h, this.p);

        GLUquadric cilindro = glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(cilindro, GLU.GLU_FILL);
        glu.gluCylinder(cilindro, ri, rs, 1, 9, 9);

        gl.glEnd();

        gl.glPopMatrix();
    }
}
