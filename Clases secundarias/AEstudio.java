/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class AEstudio {
    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float ry; //rotacion

    FCubo pared1, pared2, pared3, pared5, pared6, marco1, marco2, marco3, marco4, b2;
    FCuadrado piso_es, techo_es;
    FVidrio ventana;

    public AEstudio(GL gl, float x, float y, float z, float w, float h, float p, float ry) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.ry = ry;

        //Paredes y suelo
        this.pared1 = new FCubo(gl, 40.6f, .7f, -.404f, 4.2f, 1.57f, .0076f, 0, 0, 0, 1f, 1f, 0.8f, 0.8f, 0.4f, 0.3f);//up.x.1
        this.pared2 = new FCubo(gl, 18.5f, .7f, -.404f, 2.1f, 1.57f, .0076f, 0, 0, 0, 1f, 1f, 0.8f, 0.8f, 0.4f, 0.3f);//up.x.2
        this.pared6 = new FCubo(gl, 36.5f, 1.94f, -.404f, 20.2f, .33f, .0076f, 0, 0, 0, 1f, 1f, 0.8f, 0.8f, 0.4f, 0.3f);//up.y
        this.pared3 = new FCubo(gl, 56.6f, .7f, -.251f, .3f, 1.57f, .149f, 0, 0, 0, 1f, 1f, 0.8f, 0.8f, 0.4f, 0.3f);//der
        this.pared5 = new FCubo(gl, 29.7f, .7f, -.107f, 13.2f, 1.57f, .0076f, 0, 180, 0, 0.97f, 1f, 0.82f, 0.8f, 0.4f, 0.3f);//down
        this.piso_es = new FCuadrado(gl, 36.5f, -.87f, -0.254f, 20.3f, .147f, 90, 0, 0, 1f, 1f, 0.85f);
        this.techo_es = new FCuadrado(gl, 36.5f, 2.3f, -0.194f, 20.3f, .207f, 90, 0, 0, 1f, 1f, 0.8f);
        
        //Ventana
        this.ventana = new FVidrio(gl, 36.5f, 0.7f, -.404f, 20.2f, 1.57f, 0, 0, 0); 
        this.b2 = new FCubo(gl, 36.5f, .65f, -.404f, 19.2f, .04f, .007f, 0, 180, 0, 1f, 0.8f, 0.6f, 0.7f, 0.5f, 0.3f);//up.y

        //Marcos ventanas
        this.marco1 = new FCubo(gl, 45.1f, .69f, -.404f, .3f, 0.9f, .0079f, 0, 180, 0, 1f, 0.8f, 0.6f, 0.7f, 0.5f, 0.3f);//up.x
        this.marco2 = new FCubo(gl, 48.8f, 1.59f, -.404f, 4.f, .06f, .0079f, 0, 180, 0, 1f, 0.8f, 0.6f, 0.7f, 0.5f, 0.3f);//up.y
        this.marco3 = new FCubo(gl, 28.7f, .69f, -.404f, .3f, 0.9f, .0079f, 0, 180, 0, 1f, 0.8f, 0.6f, 0.7f, 0.5f, 0.3f);//up.x
        this.marco4 = new FCubo(gl, 32.4f, 1.59f, -.404f, 4.f, .06f, .0079f, 0, 180, 0, 1f, 0.8f, 0.6f, 0.7f, 0.5f, 0.3f);//up.y
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glScalef(this.w, this.h, this.p);

        this.pared1.x = -this.pared1.x + 64.8f;
        this.pared1.Dibuja();
        this.pared1.x = -this.pared1.x + 64.8f;
        this.pared1.Dibuja();
        this.pared2.x = -this.pared2.x + 73.3f;
        this.pared2.Dibuja();
        this.pared2.x = -this.pared2.x + 73.3f;
        this.pared2.Dibuja();
        this.pared6.y = -this.pared6.y + 1.4f;
        this.pared6.Dibuja();
        this.pared6.y = -this.pared6.y + 1.4f;
        this.pared6.Dibuja();
        this.pared3.Dibuja();
        this.pared5.Dibuja();
        this.piso_es.Dibuja();
        
        this.ventana.Dibuja();
        this.b2.Dibuja();

        this.marco1.x = -this.marco1.x + 97.6f;
        this.marco1.Dibuja();
        this.marco1.x = -this.marco1.x + 97.6f;
        this.marco1.Dibuja();
        this.marco2.y = -this.marco2.y + 1.4f;
        this.marco2.Dibuja();
        this.marco2.y = -this.marco2.y + 1.4f;
        this.marco2.Dibuja();
        
        this.marco3.x = -this.marco3.x + 64.8f;
        this.marco3.Dibuja();
        this.marco3.x = -this.marco3.x + 64.8f;
        this.marco3.Dibuja();
        this.marco4.y = -this.marco4.y + 1.4f;
        this.marco4.Dibuja();
        this.marco4.y = -this.marco4.y + 1.4f;
        this.marco4.Dibuja();
        
        this.techo_es.Dibuja();

        gl.glEnd();
        gl.glPopMatrix();

    }
}
