/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class Casa {

    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float rx, ry, rz; //rotacion

    float tx, ty, tz;

    PrimerPiso planta1;
    SegundoPiso planta2;
    FCubo linea4, linea6, linea7, linea8, piso, techo, columna, sop_manguera, generador, cielo,
            frente1, frente3, frente4, frente5, frente6, frente7, patio, valla1, valla2,
            bl1, bl2, bl3, bl4, bl5, bl6, bl7, bl8, bl9, bl10, bl11, bl12, bl13, bl14, bl15, bl16, bl17, bl18, bl19;
    FCilindro maceta;
    Arbustos planta, ar2, ar3, ar4, ar5, ar6, ar7, ar8, ar9, ar10, ar11, ar12, ar13, ar14, ar15, ar16;
    FTriangulo pin1, pin2, pin3, pin4, pin5, pin6, pin7, pin8, pin9, pin10, pin11, pin12, pin13, pin14, pin15, pin16;
    FNubes nube1, nube2, nube3, nube4, nube5;
    FEsfera sol;

    public Casa(GL gl, float x, float y, float z, float w, float h, float p, float ry) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.ry = ry;

        this.planta1 = new PrimerPiso(gl, 0, 0.1f, 0, 1, 1, 1, 0);
        this.planta2 = new SegundoPiso(gl, 0, 0.1f, 0, 1, 1, 1, 0);

        //Primer piso
        this.linea4 = new FCubo(gl, 25.35f, .95f, .2f, .04f, 1.6f, .0009f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//der.down
        this.linea6 = new FCubo(gl, -35.65f, .95f, .2f, .04f, 1.6f, .0009f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//iz.down
        this.linea7 = new FCubo(gl, 25.55f, .95f, .394f, .04f, 1.6f, .0009f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//der.frente
        this.linea8 = new FCubo(gl, -75.49f, .95f, .394f, .04f, 1.6f, .0009f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//iz.frente

        //Pared rosa
        this.frente1 = new FCubo(gl, 41.15f, -.39f, .417f, 15.66f, .35f, .025f, 0, 0, 0, .9f, .59f, .59f, .9f, .59f, .59f);//frente
        this.frente3 = new FCubo(gl, 19.f, -.39f, .196f, 6.41f, .35f, .006f, 0, 0, 0, .9f, .59f, .59f, .9f, .59f, .59f);//down.frente.1
        this.frente4 = new FCubo(gl, -15.8f, -.39f, .196f, 19.6f, .35f, .006f, 0, 0, 0, .9f, .59f, .59f, .9f, .59f, .59f);//down.frente.2
        this.frente5 = new FCubo(gl, -35.6f, -.39f, .297f, .2f, .35f, .099f, 0, 0, 0, .9f, .59f, .59f, .9f, .59f, .59f);//iz.lado.der
        this.frente6 = new FCubo(gl, -75.5f, -.39f, .247f, .2f, .35f, .147f, 0, 0, 0, .9f, .59f, .59f, .9f, .59f, .59f);//iz.lado.iz
        this.frente7 = new FCubo(gl, -72.61f, -.39f, .39f, 2.83f, .35f, .006f, 0, 0, 0, .9f, .59f, .59f, .9f, .59f, .59f);//iz.frente
        this.patio = new FCubo(gl, 41.15f, -.39f, -.437f, 15.66f, .35f, .025f, 0, 0, 0, .9f, .59f, .59f, .9f, .59f, .59f);

        //Ladrillos
        this.bl1 = new FCubo(gl, 30.15f, -.49f, .438f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//frente.1
        this.bl2 = new FCubo(gl, 32.55f, -.32f, .438f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//frente.2
        this.bl3 = new FCubo(gl, 55.61f, -.49f, .438f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//frente.3
        this.bl4 = new FCubo(gl, 42.15f, -.49f, .438f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//frente.4
        this.bl5 = new FCubo(gl, 44.55f, -.32f, .438f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//frente.5
        this.bl6 = new FCubo(gl, -72.0f, -.49f, .392f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//iz.frente.1
        this.bl7 = new FCubo(gl, -36.6f, -.39f, .392f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//iz.frente.2
        this.bl8 = new FCubo(gl, 19.f, -.32f, .198f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//down.frente.1
        this.bl9 = new FCubo(gl, -27.8f, -.32f, .198f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//down.frente.2.1
        this.bl10 = new FCubo(gl, -25.8f, -.49f, .198f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//down.frente.2.2
        this.bl11 = new FCubo(gl, -75.53f, -.49f, .184f, .18f, .087f, .011f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//iz.lado.iz.1
        this.bl12 = new FCubo(gl, -75.53f, -.32f, .207f, .18f, .087f, .011f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//iz.lado.iz.2
        this.bl13 = new FCubo(gl, -75.53f, -.32f, .344f, .18f, .087f, .011f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//iz.lado.iz.3
        this.bl14 = new FCubo(gl, -35.57f, -.49f, .244f, .18f, .087f, .011f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//der.lado.iz
        this.bl15 = new FCubo(gl, 30.15f, -.49f, -.458f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//patio.1
        this.bl16 = new FCubo(gl, 32.55f, -.32f, -.458f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//patio.2
        this.bl17 = new FCubo(gl, 55.61f, -.49f, -.458f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//patio.3
        this.bl18 = new FCubo(gl, 42.15f, -.49f, -.458f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//patio.4
        this.bl19 = new FCubo(gl, 44.55f, -.32f, -.458f, 1.21f, .087f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);//patio.5

        //Patio
        this.piso = new FCubo(gl, -9.05f, -.69f, -.479f, 18.3f, .056f, .075f, 0, 0, 0, 0.87f, 0.87f, 0.86f, 0.87f, 0.87f, 0.86f);
        this.techo = new FCubo(gl, -9.05f, 2.45f, -.481f, 18.3f, .056f, .075f, 0, 0, 0, 0.4f, 0.2f, 0.1f, 0.45f, 0.25f, 0.15f);
        this.columna = new FCubo(gl, 7.5f, .9f, -.55f, .19f, 1.54f, .003f, 0, 0, 0, 0.45f, 0.25f, 0.15f, 0.4f, 0.2f, 0.1f);
        this.valla1 = new FCubo(gl, -96.2f, -.15f, 0f, .35f, .53f, .759f, 0, 0, 0, 0.95f, 0.95f, 1f, 0.9f, 0.9f, 1f);//lados
        this.valla2 = new FCubo(gl, -5.f, -.15f, -.757f, 91.6f, .53f, .0076f, 0, 0, 0, 0.95f, 0.95f, 1f, 0.9f, 0.9f, 1f);//down
        this.maceta = new FCilindro(gl, -32.8f, -.68f, .364f, 4.f, .04f, .77f, 0, 0, 0, 0.9f, 0.6f, 0.4f, 0.57f, 0.4f);
        this.sop_manguera = new FCubo(gl, 25.35f, .02f, .348f, .61f, .07f, .005f, 0, 0, 0, .7f, .7f, .7f, .7f, .7f, .7f);
        this.generador = new FCubo(gl, 58.6f, -.39f, .051f, 2.3f, .35f, .039f, 0, 0, 180, 0.8f, 0.8f, 0.8f, 0.6f, 0.6f, 0.6f);

        //Plantas
        this.planta = new Arbustos(gl, -34.2f, .18f, .356f, 1.9f, .4f, .022f, 0, 0, 0);//maceta
        this.ar2 = new Arbustos(gl, -43.2f, -.58f, -.426f, 2.4f, .6f, .022f, 0, 0, 0);//down.1
        this.ar3 = new Arbustos(gl, -48.2f, -.58f, -.426f, 2.4f, .6f, .022f, 0, 0, 0);//down.2
        this.ar4 = new Arbustos(gl, -53.2f, -.58f, -.426f, 2.4f, .6f, .022f, 0, 0, 0);//down.3
        this.ar5 = new Arbustos(gl, -58.2f, -.58f, -.426f, 2.4f, .6f, .022f, 0, 0, 0);//down.4
        this.ar6 = new Arbustos(gl, 29.2f, .05f, .41f, 2.4f, .4f, .022f, 0, 0, 0);//frente.1
        this.ar7 = new Arbustos(gl, 34.2f, .05f, .41f, 2.4f, .4f, .022f, 0, 0, 0);//frente.2
        this.ar8 = new Arbustos(gl, 39.2f, .05f, .41f, 2.4f, .4f, .022f, 0, 0, 0);//frente.3
        this.ar9 = new Arbustos(gl, 44.2f, .05f, .41f, 2.4f, .4f, .022f, 0, 0, 0);//frente.4
        this.ar10 = new Arbustos(gl, 49.2f, .05f, .41f, 2.4f, .4f, .022f, 0, 0, 0);//frente.5
        this.ar11 = new Arbustos(gl, -2.2f, -.58f, .21f, 2.4f, .4f, .022f, 0, 0, 0);//frente.d.1
        this.ar12 = new Arbustos(gl, -7.2f, -.58f, .21f, 2.4f, .4f, .022f, 0, 0, 0);//frente.d.2
        this.ar13 = new Arbustos(gl, -2.7f, -.28f, .21f, 2.4f, .4f, .022f, 0, 0, 0);//frente.d.3
        this.ar14 = new Arbustos(gl, -67.5f, -.58f, .08f, 2.4f, .4f, .022f, 0, 0, 0);//iz.1
        this.ar15 = new Arbustos(gl, -72.5f, -.58f, .08f, 2.4f, .4f, .022f, 0, 0, 0);//iz.2
        this.ar16 = new Arbustos(gl, -67.5f, -.28f, .08f, 2.4f, .4f, .022f, 0, 0, 0);//iz.3       

        this.pin1 = new FTriangulo(gl, 29.2f, -.58f, -.47f, .015f, .57f, 0, 270, 0, 0.23f, 0.78f, 0.1f);
        this.pin2 = new FTriangulo(gl, 30.2f, -.58f, -.485f, 1.015f, .57f, 0, 0, 0, 0.2f, 0.7f, 0.1f);
        this.pin3 = new FTriangulo(gl, 29.2f, -.58f, -.5f, .015f, .57f, 0, 90, 0, 0.23f, 0.78f, 0.1f);
        this.pin4 = new FTriangulo(gl, 28.2f, -.58f, -.485f, 1.015f, .57f, 0, 180, 0, 0.2f, 0.7f, 0.1f);

        this.pin5 = new FTriangulo(gl, 36.2f, -.58f, -.47f, .015f, .57f, 0, 270, 0, 0.23f, 0.78f, 0.1f);
        this.pin6 = new FTriangulo(gl, 37.2f, -.58f, -.485f, 1.015f, .57f, 0, 0, 0, 0.2f, 0.7f, 0.1f);
        this.pin7 = new FTriangulo(gl, 36.2f, -.58f, -.5f, .015f, .57f, 0, 90, 0, 0.23f, 0.78f, 0.1f);
        this.pin8 = new FTriangulo(gl, 35.2f, -.58f, -.485f, 1.015f, .57f, 0, 180, 0, 0.2f, 0.7f, 0.1f);

        this.pin9 = new FTriangulo(gl, 45.2f, -.58f, -.47f, .015f, .57f, 0, 270, 0, 0.23f, 0.78f, 0.1f);
        this.pin10 = new FTriangulo(gl, 46.2f, -.58f, -.485f, 1.015f, .57f, 0, 0, 0, 0.2f, 0.7f, 0.1f);
        this.pin11 = new FTriangulo(gl, 45.2f, -.58f, -.5f, .015f, .57f, 0, 90, 0, 0.23f, 0.78f, 0.1f);
        this.pin12 = new FTriangulo(gl, 44.2f, -.58f, -.485f, 1.015f, .57f, 0, 180, 0, 0.2f, 0.7f, 0.1f);

        this.pin13 = new FTriangulo(gl, 54.2f, -.58f, -.47f, .015f, .57f, 0, 270, 0, 0.23f, 0.78f, 0.1f);
        this.pin14 = new FTriangulo(gl, 55.2f, -.58f, -.485f, 1.015f, .57f, 0, 0, 0, 0.2f, 0.7f, 0.1f);
        this.pin15 = new FTriangulo(gl, 54.2f, -.58f, -.5f, .015f, .57f, 0, 90, 0, 0.23f, 0.78f, 0.1f);
        this.pin16 = new FTriangulo(gl, 53.2f, -.58f, -.485f, 1.015f, .57f, 0, 180, 0, 0.2f, 0.7f, 0.1f);

        //Cielo
        this.cielo = new FCubo(gl, 0f, 0f, 0f, 251.4f, 115.6f, 4.5f, 0, 0, 0, 0.2f, 0.2f, 0.5f, 0.2f, 0.2f, 0.5f);
        this.nube1 = new FNubes(gl, 82.5f, 25.1f, .37f, 20f, 2f, .3f, 0, 0, 0);
        this.nube2 = new FNubes(gl, -82.5f, 22.1f, .27f, 28f, 2f, .3f, 0, 0, 0);
        this.nube3 = new FNubes(gl, 25.5f, 24.1f, .47f, 35f, 2f, .3f, 0, 0, 0);
        this.nube4 = new FNubes(gl, -42.5f, 23.1f, .57f, 21f, 2f, .3f, 0, 0, 0);
        this.nube5 = new FNubes(gl, 2.5f, 20.1f, 0f, 61f, 2f, .4f, 0, 0, 0);
        this.sol = new FEsfera(gl, -112.5f, 29.58f, -.28f, 18.3f, 2.5f, .2f, 0, 0, 0, 1, .9f, 0f);
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glScalef(this.w, this.h, this.p);

        this.planta1.Dibuja();
        this.planta2.Dibuja();

        this.linea4.Dibuja();
        this.linea6.Dibuja();
        this.linea7.Dibuja();
        this.linea8.Dibuja();

        this.frente1.Dibuja();
        this.frente3.Dibuja();
        this.frente4.Dibuja();
        this.frente5.Dibuja();
        this.frente6.Dibuja();
        this.frente7.x = -this.frente7.x - 111.19f;
        this.frente7.Dibuja();
        this.frente7.x = -this.frente7.x - 111.19f;
        this.frente7.Dibuja();
        this.patio.Dibuja();

        this.bl1.Dibuja();
        this.bl2.Dibuja();
        this.bl3.Dibuja();
        this.bl4.Dibuja();
        this.bl5.Dibuja();
        this.bl6.Dibuja();
        this.bl7.Dibuja();
        this.bl8.Dibuja();
        this.bl9.Dibuja();
        this.bl10.Dibuja();
        this.bl11.Dibuja();
        this.bl12.Dibuja();
        this.bl13.Dibuja();
        this.bl14.Dibuja();
        this.bl15.Dibuja();
        this.bl16.Dibuja();
        this.bl17.Dibuja();
        this.bl18.Dibuja();
        this.bl19.Dibuja();

        this.piso.Dibuja();
        this.techo.Dibuja();
        this.columna.x = -this.columna.x - 18.19f;
        this.columna.Dibuja();
        this.columna.x = -this.columna.x - 18.19f;
        this.columna.Dibuja();
        this.valla1.x = -this.valla1.x - 10;
        this.valla1.Dibuja();
        this.valla1.x = -this.valla1.x - 10;
        this.valla1.Dibuja();
        this.valla2.Dibuja();

        this.maceta.Dibuja();
        this.sop_manguera.Dibuja();
        this.generador.Dibuja();

        this.planta.Dibuja();
        this.ar2.Dibuja();
        this.ar3.Dibuja();
        this.ar4.Dibuja();
        this.ar5.Dibuja();
        this.ar6.Dibuja();
        this.ar7.Dibuja();
        this.ar8.Dibuja();
        this.ar9.Dibuja();
        this.ar10.Dibuja();
        this.ar11.Dibuja();
        this.ar12.Dibuja();
        this.ar13.Dibuja();
        this.ar14.Dibuja();
        this.ar15.Dibuja();
        this.ar16.Dibuja();

        this.pin1.Dibuja();
        this.pin2.Dibuja();
        this.pin3.Dibuja();
        this.pin4.Dibuja();

        this.pin5.Dibuja();
        this.pin6.Dibuja();
        this.pin7.Dibuja();
        this.pin8.Dibuja();

        this.pin9.Dibuja();
        this.pin10.Dibuja();
        this.pin11.Dibuja();
        this.pin12.Dibuja();

        this.pin13.Dibuja();
        this.pin14.Dibuja();
        this.pin15.Dibuja();
        this.pin16.Dibuja();

        this.cielo.Dibuja();
        this.nube1.z = -this.nube1.z - .10f;
        this.nube1.Dibuja();
        this.nube1.z = -this.nube1.z - .10f;
        this.nube1.Dibuja();
        this.nube2.z = -this.nube2.z - .09f;
        this.nube2.Dibuja();
        this.nube2.z = -this.nube2.z -.09f;
        this.nube2.Dibuja();
        this.nube3.z = -this.nube3.z - .10f;
        this.nube3.Dibuja();
        this.nube3.z = -this.nube3.z - .10f;
        this.nube3.Dibuja();
        this.nube4.z = -this.nube4.z - .08f;
        this.nube4.Dibuja();
        this.nube4.z = -this.nube4.z - .08f;
        this.nube4.Dibuja();
        this.nube5.z = -this.nube5.z - 1.5f;
        this.nube5.Dibuja();
        this.nube5.z = -this.nube5.z - 1.5f;
        this.nube5.Dibuja();
        this.sol.Dibuja();

        gl.glEnd();
        gl.glPopMatrix();

    }
}
