/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class FPrismaTriangular {

    GL gl;
    float x, y, z; //traslacion
    float w, h, d; //escalado
    float rx, ry, rz; //rotacion
    float r1, g1, b1, r2, g2, b2; //color

    public FPrismaTriangular(GL gl, float x, float y, float z, float w, float h, float d, float rx, float ry, float rz, float r1, float g1, float b1, float r2, float g2, float b2) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.d = d;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
        this.r1 = r1;
        this.g1 = g1;
        this.b1 = b1;
        this.r2 = r2;
        this.g2 = g2;
        this.b2 = b2;
    }

    public void Dibuja() {

        gl.glPushMatrix();

        gl.glColor3f(this.r1, this.g1, this.b1);
        gl.glTranslatef(x, y, z);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);

        gl.glScalef(w, h, d);

        gl.glBegin(GL.GL_QUADS);

        //Cara Lateral Izquierda
        gl.glVertex3f(0, 1, 0.5f);
        gl.glVertex3f(0, 1, -0.5f);
        gl.glVertex3f(-1, -1, -1);
        gl.glVertex3f(-1, -1, 1);

        //Cara Lateral Derecha
        gl.glVertex3f(0, 1, -0.5f);
        gl.glVertex3f(0, 1, 0.5f);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(1, -1, -1);

        gl.glColor3f(this.r2, this.g2, this.b2);
        //Cara Inferior
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(-1, -1, -1);

        gl.glEnd();

        gl.glColor3f(this.r1, this.g1, this.b1);
        gl.glBegin(GL.GL_TRIANGLES);

        //Cara Trasera
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(0, 1, -0.5f);
        gl.glVertex3f(-1, -1, -1);

        //Cara Frontal
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(0, 1, 0.5f);
        gl.glVertex3f(-1, -1, 1);

        gl.glEnd();

        gl.glPopMatrix();

//        gl.glFlush();
    }

    public void gira() {
        ry = 90;
    }
}
