/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class FNubes {
    GL gl;
    float x, y, z;
    float w, h, p;
    float rx, ry, rz;

    FEsfera nube1, nube2, nube3;

    public FNubes(GL gl, float x, float y, float z, float w, float h, float p, float rx, float ry, float rz) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;

        this.nube1 = new FEsfera(gl, 0.3f, 0, 0, 0.3f, 0.2f, 0.2f, 0, 0, 0, 1, 1, 1);
        this.nube2 = new FEsfera(gl, 0, 0, 0, 0.3f, 0.3f, 0.3f, 0, 0, 0, .95f, .95f, .95f);
        this.nube3 = new FEsfera(gl, -0.3f, 0, 0, 0.3f, 0.2f, 0.2f, 0, 0, 0, 1, 1, 1);
    }

    public void Dibuja() {
        gl.glPushMatrix();

        movimiento();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(ry, 0, 1, 0);
        gl.glScalef(this.w, this.h, this.p);

        nube1.Dibuja();
        nube2.Dibuja();
        nube3.Dibuja();

        gl.glEnd();

        gl.glPopMatrix();

    }
    
    void movimiento() {

        this.x += .5;
        if (this.x > 130) {
            this.x = -145;
        }
    }
}
