/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class SegundoPiso {

    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float rx, ry, rz; //rotacion

    FCubo dormitorio, marco1, marco2, b1, marco3, marco4, b2, marco5, marco6, b3, bal1, bal2, bal3, bal4, bal5, bal6, bal7,
            linea1, linea2, linea3, linea5, a1, tec1, tec2;
    FPrismaTriangular techo1, techo2;
    FPrismaTriIguales techo, techo3, techo4;
    FPiramideFrustum balcon;
    FVidrio puerta, ventana1, ventana2;
    FDona aro;
    FCilindro red, foco;
    FCirculo a2;

    public SegundoPiso(GL gl, float x, float y, float z, float w, float h, float p, float ry) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.ry = ry;

        //Dormitorios
        this.dormitorio = new FCubo(gl, 41.19f, 3.94f, -0.0515f, 15.62f, 1.57f, .36f, 0, 0, 0, 1f, 1f, 0.8f, 1f, 1f, 0.8f);
        this.linea1 = new FCubo(gl, 25.55f, 4.1f, .309f, .04f, 1.6f, .0009f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//up.der
        this.linea3 = new FCubo(gl, 56.85f, 4.1f, .309f, .04f, 1.6f, .0009f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//der
        this.linea2 = new FCubo(gl, 56.87f, 2.5f, -.412f, .04f, 3.2f, .0009f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//down
        this.linea5 = new FCubo(gl, 25.55f, 4.1f, -.412f, .04f, 1.6f, .0009f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//down.up.der

        //Techos
        this.techo1 = new FPrismaTriangular(gl, 41.19f, 6.9f, -0.0515f, 16.7f, 1.37f, .398f, 0, 0, 0, 0.4f, 0.2f, 0.1f, 0.4f, 0.2f, 0.1f);//dormitorios
        this.techo2 = new FPrismaTriangular(gl, -19.15f, 3.34f, -0.106f, .32f, .96f, 44.8f, 0, 0, 0, 0.4f, 0.2f, 0.1f, 1f, 1f, 0.8f);//primer piso
        this.techo = new FPrismaTriIguales(gl, 3.15f, 3.34f, -0.106f, .32f, .96f, 22.4f, 0, 0, 0, 0.4f, 0.2f, 0.1f, 1f, 1f, 0.8f);//primer piso
        this.techo3 = new FPrismaTriIguales(gl, -55.5f, 3.34f, 0.247f, 20.8f, .96f, .155f, 0, 0, 0, 0.4f, 0.2f, 0.1f, 1f, 1f, 0.8f);//garaje
        this.techo4 = new FPrismaTriIguales(gl, 8.15f, 2.2f, .25f, 4.8f, .4f, .05f, 0, 0, 0, 0.4f, 0.2f, 0.1f, 0.4f, 0.2f, 0.1f);//estrada
        this.balcon = new FPiramideFrustum(gl, 41.19f, 2.7f, 0.3515f, 16.7f, .34f, .058f, 0, 0, 0, 0.4f, 0.2f, 0.1f, 1f, 1f, 0.8f);

        this.tec1 = new FCubo(gl, -16.2f, 2.32f, -.251f, .5f, .06f, .149f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.tec2 = new FCubo(gl, -26.2f, 2.32f, -.251f, .5f, .06f, .149f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);

        //Ventanas y puertas
        this.puerta = new FVidrio(gl, 41.15f, 3.94f, .309f, 7.7f, 1.1f, 0, 0, 0);
        this.marco1 = new FCubo(gl, 33.79f, 3.94f, .304f, .3f, 1.1f, .0079f, 0, 0, 0, 1f, 1f, 1f, 0.7f, 0.5f, 0.3f);//down.x
        this.marco2 = new FCubo(gl, 41.15f, 5f, .304f, 7.7f, .06f, .0079f, 0, 0, 0, 1f, 1f, 1f, 0.7f, 0.5f, 0.3f);//down.y
        this.b1 = new FCubo(gl, 41.15f, 3.94f, .304f, .15f, 1.1f, .007f, 0, 0, 0, 1f, 1f, 1f, 0.7f, 0.5f, 0.3f);//down

        this.marco3 = new FCubo(gl, 45.1f, 3.94f, -.407f, .3f, 0.9f, .0079f, 0, 180, 0, 1f, 0.8f, 0.6f, 0.7f, 0.5f, 0.3f);//up.x//der
        this.marco4 = new FCubo(gl, 48.8f, 4.8f, -.407f, 4.f, .06f, .0079f, 0, 180, 0, 1f, 0.8f, 0.6f, 0.7f, 0.5f, 0.3f);//up.y
        this.ventana1 = new FVidrio(gl, 48.8f, 3.95f, -.412f, 4.f, .9f, 0, 0, 0);
        this.b2 = new FCubo(gl, 48.8f, 4.f, -.407f, 4.f, .04f, .007f, 0, 180, 0, 1f, 0.8f, 0.6f, 0.7f, 0.5f, 0.3f);

        this.marco5 = new FCubo(gl, 28.7f, 3.94f, -.407f, .3f, 0.9f, .0079f, 0, 180, 0, 1f, 0.8f, 0.6f, 0.7f, 0.5f, 0.3f);//up.x//iz
        this.marco6 = new FCubo(gl, 32.4f, 4.8f, -.407f, 4.f, .06f, .0079f, 0, 180, 0, 1f, 0.8f, 0.6f, 0.7f, 0.5f, 0.3f);//up.y
        this.ventana2 = new FVidrio(gl, 32.4f, 3.95f, -.412f, 4.f, .9f, 0, 0, 0);
        this.b3 = new FCubo(gl, 32.4f, 4.f, -.407f, 4.f, .04f, .007f, 0, 180, 0, 1f, 0.8f, 0.6f, 0.7f, 0.5f, 0.3f);

        //Balcon
        this.bal1 = new FCubo(gl, 25.75f, 3.4f, .386f, .19f, .4f, .003f, 0, 0, 0, 0.7f, 0.45f, 0.3f, 0.7f, 0.5f, 0.3f);//esquinas
        this.bal2 = new FCubo(gl, 25.75f, 3.4f, .345f, .19f, .4f, .003f, 0, 0, 0, 0.7f, 0.45f, 0.3f, 0.7f, 0.5f, 0.3f);//ver.lados
        this.bal3 = new FCubo(gl, 30.75f, 3.4f, .386f, .19f, .4f, .003f, 0, 0, 0, 0.7f, 0.45f, 0.3f, 0.7f, 0.5f, 0.3f);//ver.frente.1
        this.bal4 = new FCubo(gl, 35.75f, 3.4f, .386f, .19f, .4f, .003f, 0, 0, 0, 0.7f, 0.45f, 0.3f, 0.7f, 0.5f, 0.3f);//ver.frente.2
        this.bal5 = new FCubo(gl, 40.75f, 3.4f, .386f, .19f, .4f, .003f, 0, 0, 0, 0.7f, 0.45f, 0.3f, 0.7f, 0.5f, 0.3f);//ver.frente.3
        this.bal6 = new FCubo(gl, 41.08f, 3.8f, .386f, 15.49f, .04f, .003f, 0, 0, 0, 0.7f, 0.45f, 0.3f, 0.7f, 0.5f, 0.3f);//hor.frente
        this.bal7 = new FCubo(gl, 25.75f, 3.8f, .345f, .17f, .04f, .04f, 0, 0, 0, 0.7f, 0.45f, 0.3f, 0.7f, 0.5f, 0.3f);//hor.lados
        this.foco = new FCilindro(gl, 30.75f, 4.4f, .315f, 10f, .12f, .4f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.05f, 0.05f);

        //Aro
        this.aro = new FDona(gl, -55.61f, 1.94f, .418f, 4.f, .04f, .1f, 90, 0, 0, 0.3f, 0.3f, 0.3f, 0.3f, 0.06f);
        this.red = new FCilindro(gl, -55.61f, 1.94f, .418f, 4.f, .04f, .17f, 90, 0, 0, 0.5f, 0.5f, 0.5f, 0.27f, 0.27f);
        this.a1 = new FCubo(gl, -55.61f, 1.94f, .399f, 1.24f, .17f, .005f, 0, 0, 0, 1f, .3f, .4f, 1f, .3f, .4f);
        this.a2 = new FCirculo(gl, -55.61f, 1.4f, .403f, 3.4f, 1.1f, .09f, 180, 0, 0, 1, 1, 1);
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.rx, 1, 0, 0);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glRotatef(this.rz, 0, 0, 1);
        gl.glScalef(this.w, this.h, this.p);

        this.dormitorio.Dibuja();
        this.techo1.Dibuja();
        this.techo2.Dibuja();
        this.techo2.gira();
        this.techo.Dibuja();
        this.techo.gira();
        this.techo3.Dibuja();
        this.techo4.Dibuja();
        this.balcon.Dibuja();

        this.tec1.x = -this.tec1.x - 21;
        this.tec1.Dibuja();
        this.tec1.x = -this.tec1.x - 21;
        this.tec1.Dibuja();
        this.tec2.x = -this.tec2.x - 21;
        this.tec2.Dibuja();
        this.tec2.x = -this.tec2.x - 21;
        this.tec2.Dibuja();

        this.puerta.Dibuja1();
        this.marco1.x = -this.marco1.x + 82.35f;
        this.marco1.Dibuja();
        this.marco1.x = -this.marco1.x + 82.35f;
        this.marco1.Dibuja();
        this.marco2.y = -this.marco2.y + 8.1f;
        this.marco2.Dibuja();
        this.marco2.y = -this.marco2.y + 8.1f;
        this.marco2.Dibuja();
        this.b1.Dibuja();

        this.ventana1.Dibuja1();
        this.marco3.x = -this.marco3.x + 97.6f;
        this.marco3.Dibuja();
        this.marco3.x = -this.marco3.x + 97.6f;
        this.marco3.Dibuja();
        this.marco4.y = -this.marco4.y + 7.9f;
        this.marco4.Dibuja();
        this.marco4.y = -this.marco4.y + 7.9f;
        this.marco4.Dibuja();
        this.b2.Dibuja();

        this.ventana2.Dibuja1();
        this.marco5.x = -this.marco5.x + 64.8f;
        this.marco5.Dibuja();
        this.marco5.x = -this.marco5.x + 64.8f;
        this.marco5.Dibuja();
        this.marco6.y = -this.marco6.y + 7.9f;
        this.marco6.Dibuja();
        this.marco6.y = -this.marco6.y + 7.9f;
        this.marco6.Dibuja();
        this.b3.Dibuja();

        this.bal1.x = -this.bal1.x + 82.35f;
        this.bal1.Dibuja();
        this.bal1.x = -this.bal1.x + 82.35f;
        this.bal1.Dibuja();

        this.bal2.x = -this.bal2.x + 82.35f;
        this.bal2.Dibuja();
        this.bal2.x = -this.bal2.x + 82.35f;
        this.bal2.Dibuja();

        this.bal3.x = -this.bal3.x + 82.35f;
        this.bal3.Dibuja();
        this.bal3.x = -this.bal3.x + 82.35f;
        this.bal3.Dibuja();

        this.bal4.x = -this.bal4.x + 82.35f;
        this.bal4.Dibuja();
        this.bal4.x = -this.bal4.x + 82.35f;
        this.bal4.Dibuja();

        this.bal5.Dibuja();
        this.bal6.Dibuja();
        this.bal7.x = -this.bal7.x + 82.35f;
        this.bal7.Dibuja();
        this.bal7.x = -this.bal7.x + 82.35f;
        this.bal7.Dibuja();
        
        this.foco.x = -this.foco.x + 82.35f;
        this.foco.Dibuja();
        this.foco.x = -this.foco.x + 82.35f;
        this.foco.Dibuja();

        this.linea1.Dibuja();
        this.linea2.Dibuja();
        this.linea3.Dibuja();
        this.linea5.Dibuja();

        this.aro.Dibuja();
        this.red.DibujaL();
        this.a1.Dibuja();
        this.a2.DibujaMedio();

        gl.glEnd();
        gl.glPopMatrix();

    }
}
