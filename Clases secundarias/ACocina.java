/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.nlopez;

import javax.media.opengl.GL;

/**
 *
 * @author NATALY MICHELLE
 */
public class ACocina {

    GL gl;
    float x, y, z; //traslacion
    float w, h, p; //escalado
    float ry; //rotacion

    FCubo pared1, pared2, pared3, pared4, pared5, pared6, pared7, marco1, marco2, marco3, marco4, b1, b2, lin1, lin2,
            ref1, refp1, refp2, mess1, mess2, mess3, arm9, gab1, gab2, coci, cocip, cocipp, mangr, mangr2, mangc, orpos,
            marco5, marco6, puertav, linea, puertaam, puertai, marco7, marco8, repisa, filmess1, filmess2,
            calend1, calend2, f1, f2, f3, f4, f5;
    FCuadrado piso_c, cortina;
    FVidrio ventana;
    FCilindro cort, vaso1, vaso2, vaso3, vaso4;
    FEsfera cor1, cor2;
    FDona cora1, cora2, cora3;
    FMesa mesa;
    FArmarios arm1, arm2, arm3, arm4, arm5, arm6, arm7, arm8, arm10, arm11, arm12;
    FCirculo orn1, orn2, orn3, orn4, mani1, mani2, plato1, pla1, plato2, pla2, plato3, pla3;
    FEsfera m1, m2;
    FTetera tetera;

    public ACocina(GL gl, float x, float y, float z, float w, float h, float p, float ry) {
        this.gl = gl;
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.p = p;
        this.ry = ry;

        //Paredes y piso
        this.pared1 = new FCubo(gl, -63, .7f, -.15f, .3f, 1.57f, .26f, 0, 0, 0, 0.5f, 0.7f, 0.5f, 1f, 1f, 0.8f);//iz
        this.pared2 = new FCubo(gl, -59.6f, .7f, -.404f, 3.6f, 1.57f, .0076f, 0, 0, 0, 1f, 1f, 0.8f, 0.5f, 0.7f, 0.5f);//up.x
        this.pared5 = new FCubo(gl, -49.9f, 1.94f, -.404f, 6.5f, .33f, .0076f, 0, 0, 0, 1f, 1f, 0.8f, 0.5f, 0.7f, 0.5f);//up.y
        this.pared3 = new FCubo(gl, -36, .7f, -.11f, .3f, 1.57f, .14f, 0, 0, 0, 1f, 0.97f, 0.75f, 0.5f, 0.7f, 0.5f);//der.x
        this.pared7 = new FCubo(gl, -36, .7f, -.37f, .3f, 1.57f, .04f, 0, 0, 0, 1f, 0.97f, 0.75f, 0.5f, 0.7f, 0.5f);//der.x.1
        this.pared6 = new FCubo(gl, -36, 1.94f, .05f, .3f, 0.33f, .06f, 0, 0, 0, 1f, 0.97f, 0.75f, 0.5f, 0.7f, 0.5f);//der.y
        this.pared4 = new FCubo(gl, -49.5f, .7f, .106f, 13.3f, 1.57f, .0076f, 0, 0, 0, 0.5f, 0.7f, 0.5f, 1f, 1f, 0.8f);//down
        this.lin1 = new FCubo(gl, -62.99f, .7f, -.403f, .3f, 1.57f, .0076f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//iz.u
        this.lin2 = new FCubo(gl, -63f, .7f, .105f, .34f, 1.57f, .0076f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);//iz.d
        this.piso_c = new FCuadrado(gl, -49.8f, -.87f, -0.15f, 13.44f, .26f, 90, 0, 0, 0.92f, 0.85f, 0.6f);

        //Filo inferior
        this.f1 = new FCubo(gl, -62.9f, -.82f, -.15f, .3f, .06f, .26f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//iz
        this.f2 = new FCubo(gl, -59.6f, -.82f, -.403f, 3.6f, .06f, .0076f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//up.x
        this.f3 = new FCubo(gl, -36, -.82f, -.11f, .5f, .08f, .14f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//der.x
        this.f4 = new FCubo(gl, -36, -.82f, -.37f, .5f, .08f, .04f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//der.x.1
        this.f5 = new FCubo(gl, -49.5f, -.82f, .105f, 13.3f, .06f, .0076f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//down

        //Ventana
        this.ventana = new FVidrio(gl, -49.9f, 0.7f, -.404f, 6.5f, 1.57f, 0, 0, 0);
        this.b1 = new FCubo(gl, -51.89f, .69f, -.404f, .15f, 0.9f, .007f, 0, 180, 0, .9f, 1f, .8f, 0.7f, 0.5f, 0.3f);//up.x
        this.b2 = new FCubo(gl, -49.9f, .65f, -.404f, 6.2f, .04f, .007f, 0, 180, 0, .9f, 1f, .8f, 0.7f, 0.5f, 0.3f);//up.y

        //Marcos puertas y puertas
        this.marco1 = new FCubo(gl, -36, .38f, .03f, .45f, 1.27f, .0019f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//der.x
        this.marco2 = new FCubo(gl, -36, 1.59f, .066f, .4f, .059f, .036f, 0, 0, 0, 1f, 1f, 1f, 1f, 1f, 1f);//der.y
        this.marco5 = new FCubo(gl, -55.99f, .38f, .106f, .3f, 1.27f, .0079f, 0, 180, 0, .9f, 1f, .8f, .9f, 1f, .8f);//down.x
        this.marco6 = new FCubo(gl, -49.8f, 1.59f, .106f, 6.5f, .06f, .0079f, 0, 180, 0, .9f, 1f, .8f, .9f, 1f, .8f);//down.y
        this.puertav = new FCubo(gl, -49.8f, .38f, .106f, 6.5f, 1.27f, .00775f, 0, 180, 0, 0.4f, 0.7f, 0.4f, 0.4f, 0.7f, 0.4f);//down
        this.puertaam = new FCubo(gl, -52.71f, .38f, .106f, 1.5f, 1f, .0078f, 0, 180, 0, 0.9f, 1f, 0.6f, 0.9f, 1f, 0.6f);
        this.linea = new FCubo(gl, -49.8f, .38f, .106f, .04f, 1.27f, .0078f, 0, 0, 0, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f, 0.7f);
        this.puertai = new FCubo(gl, -63, .35f, .025f, .42f, 1.22f, .038f, 0, 180, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);//iz
        this.marco7 = new FCubo(gl, -63, .38f, .064f, .45f, 1.27f, .0019f, 0, 0, 0, .9f, 1f, .8f, 0.7f, 0.5f, 0.3f);//iz.x
        this.marco8 = new FCubo(gl, -63, 1.59f, .025f, .4f, .059f, .038f, 0, 0, 0, .9f, 1f, .8f, 0.7f, 0.5f, 0.3f);//iz.y
        this.m1 = new FEsfera(gl, -63.5f, .35f, -.008f, .4f, .05f, .004f, 0, 0, 0, 0.9f, 1.f, 0.1f);
        this.m2 = new FEsfera(gl, -62.5f, .35f, -.008f, .4f, .05f, .004f, 0, 0, 0, 0.8f, 0.8f, 0.8f);

        //Marcos ventanas
        this.marco3 = new FCubo(gl, -55.99f, .69f, -.404f, .3f, 0.9f, .0079f, 0, 180, 0, .9f, 1f, .8f, 0.7f, 0.5f, 0.3f);//up.x
        this.marco4 = new FCubo(gl, -49.8f, 1.59f, -.404f, 6.5f, .06f, .0079f, 0, 180, 0, .9f, 1f, .8f, 0.7f, 0.5f, 0.3f);//up.y

        //Detalles
            //Cortina
        this.cort = new FCilindro(gl, -57.4f, 1.7f, -.39f, .2f, .01f, 15.15f, 0, 90, 0, 0.7f, 0.7f, 0.7f, 0.3f, 0.3f);
        this.cor1 = new FEsfera(gl, -57.45f, 1.7f, -.39f, .6f, .06f, .006f, 0, 0, 0, 0.6f, 0.6f, 0.6f);
        this.cor2 = new FEsfera(gl, -41.9f, 1.7f, -.39f, .6f, .06f, .006f, 0, 0, 0, 0.6f, 0.6f, 0.6f);
        this.cora1 = new FDona(gl, -55.61f, 1.7f, -.39f, .01f, .1f, 1.1f, 0, 90, 0, 1f, 1f, 0.4f, 0.7f, 0.1f);
        this.cora2 = new FDona(gl, -54.61f, 1.7f, -.39f, .01f, .1f, 1.1f, 0, 90, 0, 1f, 1f, 0.4f, 0.7f, 0.1f);
        this.cora3 = new FDona(gl, -53.61f, 1.7f, -.39f, .01f, .1f, 1.1f, 0, 90, 0, 1f, 1f, 0.4f, 0.7f, 0.1f);
        this.cortina = new FCuadrado(gl, -54.71f, 0.6f, -.39f, 1.84f, 1.07f, 0, 0, 0, 0.9f, 1f, 0.5f);

            //Mesa
        this.mesa = new FMesa(gl, -43.7f, 2.5f, -.375f, .43f, 1.44f, .008f, 0, 0, 0);

            //Refrigeradora
        this.ref1 = new FCubo(gl, -38.2f, 0f, -.030f, 1.81f, 1.4f, .028f, 0, 0, 0, 0.95f, 0.95f, 0.95f, 0.9f, 0.9f, 0.9f);
        this.refp1 = new FCubo(gl, -40.2f, 1.03f, -.030f, .21f, .36f, .028f, 0, 0, 0, 0.9f, 0.9f, 0.9f, 0.95f, 0.95f, 0.95f);
        this.refp2 = new FCubo(gl, -40.2f, -.1f, -.030f, .21f, .73f, .028f, 0, 0, 0, 0.9f, 0.9f, 0.9f, 0.95f, 0.95f, 0.95f);
        this.mangr = new FCubo(gl, -40.5f, .97f, -.05f, .21f, .13f, .001f, 0, 0, 0, 0.15f, 0.15f, 0.15f, 0.15f, 0.15f, 0.15f);
        this.mangr2 = new FCubo(gl, -40.5f, .37f, -.05f, .21f, .13f, .001f, 0, 0, 0, 0.15f, 0.15f, 0.15f, 0.15f, 0.15f, 0.15f);

            //Meson derecho
        this.mess1 = new FCubo(gl, -38.f, -.1f, -.18f, 1.71f, .05f, .05f, 0, 0, 0, 0.95f, 0.95f, 0.95f, 0.9f, 0.9f, 0.9f);
        this.filmess1 = new FCubo(gl, -36.2f, .01f, -.18f, .21f, .12f, .05f, 0, 0, 0, 0.95f, 0.95f, 0.95f, 0.9f, 0.9f, 0.9f);
        this.arm1 = new FArmarios(gl, -40.2f, 1.85f, -.157f, .15f, 1.24f, .0047f, 0, 180, 0);
        this.arm2 = new FArmarios(gl, -40.2f, 3.85f, -.157f, .15f, 1.24f, .0047f, 0, 180, 0);
        this.arm3 = new FArmarios(gl, -40.2f, 3.85f, -.095f, .15f, 1.24f, .0047f, 0, 180, 0);

            //Meson izquierdo
        this.mess2 = new FCubo(gl, -61.4f, -.1f, -.147f, 1.71f, .05f, .1f, 0, 0, 0, 0.95f, 0.95f, 0.95f, 0.9f, 0.9f, 0.9f);
        this.filmess2 = new FCubo(gl, -62.8f, .01f, -.147f, .21f, .12f, .1f, 0, 0, 180, 0.95f, 0.95f, 0.95f, 0.9f, 0.9f, 0.9f);
        this.arm4 = new FArmarios(gl, -59.2f, 1.85f, -.207f, .15f, 1.24f, .007f, 0, 0, 0);
        this.arm5 = new FArmarios(gl, -59.2f, 1.85f, -.11f, .15f, 1.24f, .007f, 0, 0, 0);
        this.arm6 = new FArmarios(gl, -59.2f, 3.85f, -.216f, .15f, 1.24f, .0047f, 0, 0, 0);
        this.arm7 = new FArmarios(gl, -59.2f, 3.85f, -.1535f, .15f, 1.24f, .0047f, 0, 0, 0);
        this.arm8 = new FArmarios(gl, -59.2f, 3.85f, -.091f, .15f, 1.24f, .0047f, 0, 0, 0);
        this.arm10 = new FArmarios(gl, -60.5f, 1.84f, -.216f, .08f, 0.44f, .0047f, 0, 0, 0);
        this.arm11 = new FArmarios(gl, -60.5f, 1.84f, -.1535f, .08f, 0.44f, .0047f, 0, 0, 0);
        this.arm12 = new FArmarios(gl, -60.5f, 1.84f, -.091f, .08f, 0.44f, .0047f, 0, 0, 0);

            //Meson up
        this.mess3 = new FCubo(gl, -41.6f, -.1f, -.213f, 5.5f, .05f, .019f, 0, 0, 0, 0.95f, 0.95f, 0.95f, 0.9f, 0.9f, 0.9f);
        this.arm9 = new FCubo(gl, -41.6f, -.5f, -.213f, 5.5f, .36f, .019f, 180, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.gab1 = new FCubo(gl, -41.9f, -.5f, -.212f, 1.4f, .3f, .019f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.gab2 = new FCubo(gl, -45.2f, -.5f, -.212f, 1.4f, .3f, .019f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.mani1 = new FCirculo(gl, -42.7f, -.5f, -.192f, .2f, .05f, .036f, 0, 0, 0, 0.8f, 0.8f, 0.8f);
        this.mani2 = new FCirculo(gl, -44.37f, -.5f, -.192f, .2f, .05f, .036f, 0, 0, 0, 0.8f, 0.8f, 0.8f);

            //Cocina
        this.coci = new FCubo(gl, -38.2f, -.35f, -.093f, 1.81f, .5f, .03f, 0, 0, 0, 0.15f, 0.15f, 0.15f, 0.1f, 0.1f, 0.1f);
        this.cocip = new FCubo(gl, -38.4f, -.35f, -.093f, 1.81f, .4f, .025f, 0, 180, 0, 0.15f, 0.15f, 0.15f, 0.1f, 0.1f, 0.1f);
        this.cocipp = new FCubo(gl, -38.6f, -.35f, -.093f, 1.81f, .18f, .02f, 0, 0, 0, 0.15f, 0.15f, 0.15f, 0.1f, 0.1f, 0.1f);
        this.mangc = new FCubo(gl, -38.6f, -.01f, -.093f, 1.81f, .025f, .02f, 0, 0, 0, 0.8f, 0.8f, 0.8f, 0.85f, 0.85f, 0.85f);
        this.orn1 = new FCirculo(gl, -39.3f, .19f, -.081f, .6f, .009f, .036f, 90, 0, 0, 0.8f, 0.8f, 0.8f);
        this.orn2 = new FCirculo(gl, -39.3f, .19f, -.105f, .6f, .009f, .036f, 90, 0, 0, 0.8f, 0.8f, 0.8f);
        this.orn3 = new FCirculo(gl, -37.8f, .19f, -.081f, .6f, .009f, .036f, 90, 0, 0, 0.8f, 0.8f, 0.8f);
        this.orn4 = new FCirculo(gl, -37.8f, .19f, -.105f, .6f, .009f, .036f, 90, 0, 0, 0.8f, 0.8f, 0.8f);
        this.orpos = new FCubo(gl, -36.2f, .23f, -.093f, .21f, .36f, .03f, 0, 0, 0, 0.15f, 0.15f, 0.15f, 0.1f, 0.1f, 0.1f);

            //Decoracion
        this.tetera = new FTetera(gl, -61f, .19f, -.095f, .01f, .3f, .95f, 0, 90, 0, .75f, 0.75f, .75f);
        this.repisa = new FCubo(gl, -61.4f, 1.0f, -.357f, .71f, .025f, .025f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
        this.plato1 = new FCirculo(gl, -61.4f, 1.18f, -.357f, .005f, .14f, .36f, 0, 90, 0, 1f, 1f, 1f);
        this.pla1 = new FCirculo(gl, -61.3f, 1.18f, -.357f, .0025f, .07f, .36f, 0, 90, 0, 0.95f, 0.95f, 0.98f);
        this.plato2 = new FCirculo(gl, -61.4f, 1.18f, -.372f, .005f, .14f, .36f, 0, 90, 0, 1f, 1f, 1f);
        this.pla2 = new FCirculo(gl, -61.3f, 1.18f, -.372f, .0025f, .07f, .36f, 0, 90, 0, 0.95f, 0.95f, 0.98f);
        this.plato3 = new FCirculo(gl, -61.4f, 1.18f, -.342f, .005f, .14f, .36f, 0, 90, 0, 1f, 1f, 1f);
        this.pla3 = new FCirculo(gl, -61.3f, 1.18f, -.342f, .0025f, .07f, .36f, 0, 90, 0, 0.95f, 0.95f, 0.98f);
        this.vaso1 = new FCilindro(gl, -61f, -0.05f, -.13f, .9f, .01f, .1f, 0, 0, 0, 1f, 0.9f, 0.7f, 0.3f, 0.2f);
        this.vaso2 = new FCilindro(gl, -61f, -0.05f, -.143f, .9f, .01f, .1f, 0, 0, 0, 1f, 0.9f, 0.7f, 0.3f, 0.2f);
        this.vaso3 = new FCilindro(gl, -60f, -0.05f, -.143f, .9f, .01f, .1f, 0, 0, 0, 1f, 0.9f, 0.7f, 0.3f, 0.2f);
        this.vaso4 = new FCilindro(gl, -60f, -0.05f, -.13f, .9f, .01f, .1f, 0, 0, 0, 1f, 0.9f, 0.7f, 0.3f, 0.2f);
        this.calend1 = new FCubo(gl, -62.5f, 1f, -.297f, .21f, .225f, .007f, 0, 0, 0, 0.9f, 0.9f, 0.99f, 0.9f, 0.9f, 0.99f);
        this.calend2 = new FCubo(gl, -62.51f, 1f, -.297f, .21f, .255f, .009f, 0, 0, 0, 0.75f, 0.55f, 0.35f, 0.7f, 0.5f, 0.3f);
    }

    public void Dibuja() {
        gl.glPushMatrix();

        gl.glTranslatef(this.x, this.y, this.z);
        gl.glRotatef(this.ry, 0, 1, 0);
        gl.glScalef(this.w, this.h, this.p);

        this.pared1.Dibuja();
        this.pared2.x = -this.pared2.x - 99.5f;
        this.pared2.Dibuja();
        this.pared2.x = -this.pared2.x - 99.5f;
        this.pared2.Dibuja();
        this.pared5.y = -this.pared5.y + 1.4f;
        this.pared5.Dibuja();
        this.pared5.y = -this.pared5.y + 1.4f;
        this.pared5.Dibuja();
        this.pared3.Dibuja();
        this.pared7.Dibuja();
        this.pared6.z = -this.pared6.z - .25f;
        this.pared6.Dibuja();
        this.pared6.z = -this.pared6.z - .25f;
        this.pared6.Dibuja();
        this.pared4.Dibuja();
        this.piso_c.Dibuja();

        this.f1.Dibuja();
        this.f2.x = -this.f2.x - 99.5f;
        this.f2.Dibuja();
        this.f2.x = -this.f2.x - 99.5f;
        this.f2.Dibuja();
        this.f3.Dibuja();
        this.f4.Dibuja();
        this.f5.Dibuja();

        this.lin1.Dibuja();
        this.lin2.Dibuja();

        this.ventana.Dibuja();
        this.b1.x = -this.b1.x - 99.6f;
        this.b1.Dibuja();
        this.b1.x = -this.b1.x - 99.6f;
        this.b1.Dibuja();
        this.b2.Dibuja();

        this.marco1.z = -this.marco1.z + .13f;
        this.marco1.Dibuja();
        this.marco1.z = -this.marco1.z + .13f;
        this.marco1.Dibuja();
        this.marco2.Dibuja();
        this.marco3.x = -this.marco3.x - 99.6f;
        this.marco3.Dibuja();
        this.marco3.x = -this.marco3.x - 99.6f;
        this.marco3.Dibuja();
        this.marco4.y = -this.marco4.y + 1.4f;
        this.marco4.Dibuja();
        this.marco4.y = -this.marco4.y + 1.4f;
        this.marco4.Dibuja();
        this.marco5.x = -this.marco5.x - 99.6f;
        this.marco5.Dibuja();
        this.marco5.x = -this.marco5.x - 99.6f;
        this.marco5.Dibuja();
        this.marco6.Dibuja();

        this.puertav.Dibuja();
        this.puertaam.x = -this.puertaam.x - 99.7f;
        this.puertaam.Dibuja();
        this.puertaam.x = -this.puertaam.x - 99.7f;
        this.puertaam.Dibuja();
        this.linea.Dibuja();

        this.puertai.Dibuja();
        this.marco7.z = -this.marco7.z + .05f;
        this.marco7.Dibuja();
        this.marco7.z = -this.marco7.z + .05f;
        this.marco7.Dibuja();
        this.marco8.Dibuja();
        this.m1.Dibuja();
        this.m2.Dibuja();

        this.cort.Dibuja();
        this.cor1.Dibuja();
        this.cor2.Dibuja();
        this.cora1.x = -this.cora1.x - 99.7f;
        this.cora1.Dibuja();
        this.cora1.x = -this.cora1.x - 99.7f;
        this.cora1.Dibuja();
        this.cora2.x = -this.cora2.x - 99.7f;
        this.cora2.Dibuja();
        this.cora2.x = -this.cora2.x - 99.7f;
        this.cora2.Dibuja();
        this.cora3.x = -this.cora3.x - 99.7f;
        this.cora3.Dibuja();
        this.cora3.x = -this.cora3.x - 99.7f;
        this.cora3.Dibuja();
        this.cortina.x = -this.cortina.x - 99.7f;
        this.cortina.Dibuja();
        this.cortina.x = -this.cortina.x - 99.7f;
        this.cortina.Dibuja();

        this.mesa.Dibuja();

        this.ref1.Dibuja();
        this.refp1.Dibuja();
        this.refp2.Dibuja();
        this.mess1.Dibuja();
        this.arm1.Dibuja1();
        this.arm2.Dibuja2();
        this.arm3.Dibuja2();
        this.mess2.Dibuja();
        this.arm4.Dibuja1();
        this.arm5.Dibuja1();
        this.arm6.Dibuja2();
        this.arm7.Dibuja2();
        this.arm8.Dibuja2();
        this.arm10.Dibuja2();
        this.arm11.Dibuja2();
        this.arm12.Dibuja2();
        this.mess3.Dibuja();
        this.arm9.Dibuja();
        this.gab1.Dibuja();
        this.gab2.Dibuja();
        this.mani1.Dibuja();
        this.mani2.Dibuja();
        this.filmess1.Dibuja();
        this.filmess2.Dibuja();

        this.coci.Dibuja();
        this.cocip.Dibuja();
        this.cocipp.Dibuja();
        this.mangr.Dibuja();
        this.mangr2.Dibuja();
        this.mangc.Dibuja();
        this.orn1.Dibuja();
        this.orn2.Dibuja();
        this.orn3.Dibuja();
        this.orn4.Dibuja();
        this.orpos.Dibuja();

        this.tetera.Dibuja();
        this.repisa.Dibuja();
        this.plato1.Dibuja();
        this.pla1.Dibuja();
        this.plato2.Dibuja();
        this.pla2.Dibuja();
        this.plato3.Dibuja();
        this.pla3.Dibuja();
        this.vaso1.DibujaTr();
        this.vaso2.DibujaTr();
        this.vaso3.DibujaTr();
        this.vaso4.DibujaTr();
        this.calend1.Dibuja();
        this.calend2.Dibuja();

        gl.glEnd();
        gl.glPopMatrix();

    }
}
