# proyectofinal

Entrega del proyecto final de la materia de Programación Gráfica I:

Recorrido virtual 3D de la casa Rick y Morty, un programa de television. 

Este proyecto cuenta con la clase principal llamada:

- ProyectoFinal

y con 32 subclases, de las cuales 4 hacen uso de las librerías de GLU y GLUT para las primitivas en tres dimensiones:

1. Cilindro
2. Esfera
3. Dona
4. Tetera

Posee 5 vistas de camara:

1. Cámara que es controlada por teclado la posicion del personaje (Morty Smith)
2. Vista Cenital del escenario completo
3. Cámara que gira al rededor de un objeto (Casa de la familia Smith)
4. Cámara que sigue a un objeto (Nave de Rick Sanchez)
5. Cámara panorámica

Se realizaron transformaciones de los objetos como son la traslación, el escalamiento y la rotación; y las diferentes clases contienen la realizacion de los elementos del escenario, además de la gestion de los eventos del teclado y de mouse.